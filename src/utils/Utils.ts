export class Utils {

	static async sleep(ms): Promise<any>{
		return new Promise(resolve=>{
			setTimeout(resolve,ms)
		})
	}

}
