export class StringHelper {

	static isANumber(str: string){
		return !/\D/.test(str);
	}

}
