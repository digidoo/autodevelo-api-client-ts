import {IConnection} from "./IConnection";
import {HttpConnection} from "./HttpConnection";

export class BaseClient {

	public static readonly API_GRAPHQL = '/api/v-2/graphql'; // old=/api/graphql   new=/api/v-2/graphql

	private clientId: number;
	private clientSecret: string;
	private url: string;
	private conneciton: IConnection;


	public constructor(url: string, clientId: number, clientSecret: string) {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.url = url;
	}


	protected dateToString(value: Date): string {
		let month = String(value.getMonth() + 1);
		let day = String(value.getDate());
		let hours = String(value.getHours());
		let minutes = String(value.getMinutes());

		if (month.length === 1) {
			month = "0" + month;
		}

		if (day.length === 1) {
			day = "0" + day;
		}

		if (hours.length === 1) {
			hours = "0" + hours;
		}

		if (minutes.length === 1) {
			minutes = "0" + minutes;
		}

		return value.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
	}


	protected getConnection(): IConnection {
		if (this.conneciton === undefined) {
			this.conneciton = new HttpConnection(this.url, this.clientId, this.clientSecret);
		}

		return this.conneciton;
	}

}
