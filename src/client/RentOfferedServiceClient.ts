import {BaseClient} from "./BaseClient";
import {RentOfferedService} from "./entity";

export class RentOfferedServiceClient extends BaseClient {

	public async getAll(select: string): Promise<Array<RentOfferedService>> {
		return new Promise((resolve, reject) => {

			const query = 'query { rent { readRentOfferedService { ' + select + ' } } }';

			return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query})
				.then((data: object) => {
					let offeredServices = new Array<RentOfferedService>();
					data['data']['rent']['readRentOfferedService'].map(data => {
						offeredServices.push(new RentOfferedService(data))
					});
					resolve(offeredServices);
				}).catch(err => {
					reject(err);
				})
		});
	}


}
