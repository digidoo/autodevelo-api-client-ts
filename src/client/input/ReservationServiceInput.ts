
export class ReservationServiceInput  {

	count: number;
	idService: number;
	priceDay?: number;
	priceDayTax?: number;

	public constructor(idService: number, count: number, priceDay?: number, priceDayTax?: number) {
		this.idService = idService;
		this.count = count;
		this.priceDay = priceDay;
		this.priceDayTax = priceDayTax;
	}

}
