import {BaseClient} from "./BaseClient";
import {GraphqlQueryBuilder} from "../Graphql/GraphqlQueryBuilder";
import {RentReservation} from "./entity/RentReservation";
import {ReservationServiceInput} from "./input/ReservationServiceInput";

export class RentReservationClient extends BaseClient {


	public async readReservation(select: string, id: number) {
		let query = GraphqlQueryBuilder.createGplGuery(
			'rent.getReserveById',
			[select],
			{
				id: id
			}
		);

		return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query}).then((data) => {
			return new RentReservation(data['data']['rent']['getReserveById']);
		});
	}

	public async readConfirmedReservations(select: Array<string>, carId: number, from: Date, to: Date): Promise<Array<RentReservation>> {
		return this.readReservations(select, carId, from, to, true);
	}


	public async readUnconfirmedReservations(select: Array<string>, carId: number, from: Date, to: Date): Promise<Array<RentReservation>> {
		return this.readReservations(select, carId, from, to, false);
	}

	public async downloadTocPdf(reserveId: number) {
		const query = 'query { rent { getTocPdf(id: ' + reserveId + ') } }';

		return this.getConnection().authorizedDownload(BaseClient.API_GRAPHQL, {
			query: query,
		});
	}

	public async downloadReservePdf(reserveId: number) {
		const query = 'query { rent { getReservePdf(id: ' + reserveId + ') } }';

		return this.getConnection().authorizedDownload(BaseClient.API_GRAPHQL, {
			query: query,
		});
	}

	public async downloadInvoicePdf(reserveId: number, invoiceId: number) {
		const query = 'query { rent { getInvoicePdf(id: ' + reserveId + ', invoiceId: ' + invoiceId + ' ) } }';

		return this.getConnection().authorizedDownload(BaseClient.API_GRAPHQL, {
			query: query,
		});
	}


	public async createReservation(
		select: string,
		carId: number,
		services: Array<ReservationServiceInput>,
		destinationCountryId: number,
		locationRentId: number,
		locationReturnId: number,
		travelersCount: number,
		freeParking: boolean,
		rentFrom: Date,
		rentTo: Date,
		clientCountryId: number,
		clientCompany: string,
		clientName: string,
		clientSurname: string,
		clientStreet: string,
		clientNumber: string,
		clientCity: string,
		clientZip: string,
		clientEmail: string,
		clientPhone: string,
		payTypeId: number,
		payTypeDepositId: number,
		payTypeBailId: number,
		isCarDelivery: boolean,
		isCarTakeover: boolean,
		clientIc?: string,
		note?: string,
		clientNameDelivery?: string,
		clientStreetDelivery?: string,
		clientCityDelivery?: string,
		clientZipDelivery?: string
	): Promise<RentReservation> {
		return new Promise((resolve, reject) => {
			let args = {};

			args['rentFrom'] = this.dateToString(rentFrom);
			args['rentTo'] = this.dateToString(rentTo);
			args['idCar'] = carId;
			args['idDestination'] = destinationCountryId;
			args['idLocationRent'] = locationRentId;
			args['idLocationReturn'] = locationReturnId;

			args['freeParking'] = freeParking;
			args['travelers'] = travelersCount;
			args['idPayType'] = payTypeId;
			args['idPayTypeDeposit'] = payTypeDepositId;
			args['idPayTypeBail'] = payTypeBailId;
			args['isCarDelivery'] = isCarDelivery;
			args['isCarTakeover'] = isCarTakeover;
			args['note'] = note;

			args['services'] = services;

			args['client'] = {};
			args['client']['idCountry'] = clientCountryId;
			args['client']['company'] = clientCompany;
			args['client']['name'] = clientName;
			args['client']['surname'] = clientSurname;
			args['client']['street'] = clientStreet;
			args['client']['number'] = clientNumber;
			args['client']['city'] = clientCity;
			args['client']['zip'] = clientZip;
			args['client']['email'] = clientEmail;
			args['client']['phone'] = clientPhone;

			args['client']['ic'] = clientIc;
			args['client']['nameDelivery'] = clientNameDelivery;
			args['client']['streetDelivery'] = clientStreetDelivery;
			args['client']['cityDelivery'] = clientCityDelivery;
			args['client']['zipDelivery'] = clientZipDelivery;

			let query = 'mutation($input: ReservationInput!) { ' +
				'rentMutation {' +
					'createReservation(input: $input) { ' +
						select +
					' } ' +
				' } ' +
			'}';


			return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {
				query: query,
				variables: JSON.stringify({input: args})
			})
				.then((data: object) => {
					resolve(new RentReservation(data['data']['rentMutation']['createReservation']));
				}).catch(err => {
					reject(err);
				})
		});
	}


	protected async readReservations(select: Array<string>, carId: number, from: Date, to: Date, confirmed: boolean): Promise<Array<RentReservation>> {
		return new Promise((resolve, reject) => {

			let args = {
				confirmed: confirmed,
				carId: carId,
				// car: {
				// 	rentServices: {
				// 		image: {
				// 			width: 256,
				// 			height: 256
				// 		}
				// 	}
				// }
			};

			if (from !== undefined) {
				args['from'] = this.dateToString(from);
			}

			if (to !== undefined) {
				args['to'] = this.dateToString(to);
			}

			const query = GraphqlQueryBuilder.createGplGuery(
				'rent.readReservationsByCar',
				select,
				args
			);
			console.log(query);

			return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query})
				.then((data: object) => {
					const cars = this.createRentCarsFromData(data);
					resolve(cars);
				}).catch(err => {
					reject(err);
				})
		})
	}


	protected createRentCarsFromData(data: object): Array<RentReservation> {
		let reservations = new Array<RentReservation>();
		data['data']['rent']['readReservationsByCar'].map((value) => {
			let reservation = new RentReservation(value);
			reservations.push(reservation);
		});
		return reservations;
	}


}
