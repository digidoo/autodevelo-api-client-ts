import {AuthResponse} from "./entity/AuthResponse";
import {IConnection} from "./IConnection";

export abstract class WsConnection implements IConnection {

	private ws?: WebSocket;
	private _accessToken;
	private _refreshToken;
	private clientId: number;
	private clientSecret: string;
	private wsUrl: string;

	// @todo ukladat do nejakoho sessionStorage? nebo to nema smysl? kdyz nekdo zmackne f5 tak by se zbytecne delal novy access token

	public constructor(wsUrl: string, clientId: number, clientSecret: string) {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.wsUrl = wsUrl;
		this._accessToken = null;
		this._refreshToken = null;
	}


	public async send(route: string, message: object): Promise<{}> {
		return new Promise((resolve, reject) => {
			const data = [
				7,
				route,
				message
			];

			const connection = this.getConnection();

			connection.onmessage = function (msg) {
				const data = JSON.parse(msg.data);
				if (data[1] === route) {
					if (data[2]['errors'] !== undefined) {
						reject(data[2]['errors']);
					}
					resolve(data[2]);
				}
			};

			connection.onerror = function (err) {
				reject(err);
			};

			connection.send(JSON.stringify(data));
		});
	}

	authorizedDownload(route: string, message: object) {
		throw new Error('Not implemented')
	}



	public async sendAuthorizationRequest(route: string, query: object): Promise<{}> {
		let message = query;

		if (this._accessToken === null) {
			let authResponse = await this.authorization(this.clientId, this.clientSecret);

			if (authResponse.accessToken === undefined) {
				throw new Error('Error'); // @todo vyhodit pres reject
			}

			// @todo dodelat refreshToken - cekat az server vrati odpoved ze vyprsela platnos a vygenerovat novy

			this._accessToken = authResponse.accessToken;
			this._refreshToken = authResponse.refreshToken;
		}


		message['headers'] = {
			'Authorization': {
				access_token: 'Bearer ' + this._accessToken,
			}
		};

		return this.send(route, message);
	}



	protected async authorization(clientId: number, clientSecret: string): Promise<AuthResponse> {
		return new Promise((resolve, reject) => {
			this.getConnection().onopen = () => {
				this.send('/graphql/auth',{
					"body": {
						grant_type: 'client_credentials',
						client_id: clientId,
						client_secret: clientSecret,
					}
				}).then((data) => {
					resolve(new AuthResponse(
						data['access_token'],
						data['refresh_token'],
						data['token_type'],
						data['expires_in']
					))
				}).catch((data) => {
					reject(data);
				});
			};
		})
	}


	protected async refreshToken(clientId: number, refreshToken: string) {
		return this.send('/graphql/auth', {
			grant_type: 'refresh_token',
			refresh_token: refreshToken,
			client_id: clientId,
		})
	}




	protected getConnection(): WebSocket {
		if (this.ws === undefined) {
			this.ws = new WebSocket(this.wsUrl);
		}
		return this.ws;
	}


}
