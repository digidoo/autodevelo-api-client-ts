import {BaseClient} from "./BaseClient";
import {Country} from "./entity";

export class CountryClient extends BaseClient {


	public async getAll(select: string): Promise<Array<Country>> {
		return new Promise((resolve, reject) => {

			const query = 'query { countries { ' + select + ' } }';

			return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query})
				.then((carData: object) => {
					let countries = new Array<Country>();
					carData['data']['countries'].map(data => {
						countries.push(new Country(data))
					});
					resolve(countries);
				}).catch(err => {
					reject(err);
				})
		});
	}


}
