import {BaseClient} from "./BaseClient";
import {Car} from "./entity/Car";
import {GraphqlQueryBuilder} from "../Graphql/GraphqlQueryBuilder";
import {CarList} from "./entity/CarList";
import {RentCarType} from "./entity/RentCarType";
import {CarLocation, Equipment, RentReservation} from "./entity";
import {RentSettings} from "./entity/RentSettings";

export class RentClient extends BaseClient {

	public async readRentSettings(select: string): Promise<RentSettings> {
		const query = 'query { rent { getRentSettings { ' + select + ' } } }';
		return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query}).then((data) => {
			return new RentSettings(data['data']['rent']['getRentSettings']);
		});
	}


	public async readAvailableCarLocations(select: string): Promise<Array<CarLocation>> {
		const query = 'query { rent { readAvailableCarLocations { ' + select + ' } } }';
		return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query}).then((data) => {
			let locations = [];
			data['data']['rent']['readAvailableCarLocations'].map((data) => {
				locations.push(new CarLocation(data));
			});
			return locations;
		});
	}


	public async readAvailableRentCarTypes(select: string): Promise<Array<RentCarType>> {
		const query = 'query { rent { readAvailableRentCarTypes { ' + select + ' } } }';
		return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query}).then((data) => {
			let rentCarTypes = [];
			data['data']['rent']['readAvailableRentCarTypes'].map((data) => {
				rentCarTypes.push(new RentCarType(data));
			});
			return rentCarTypes;
		});
	}


	public async readAvailableRentCarEquipments(select: string) {
		const query = 'query { rent { readAvailableCarEquipments { ' + select + ' } } }';
		return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query}).then((data) => {
			let equipments = [];
			data['data']['rent']['readAvailableCarEquipments'].map((data) => {
				equipments.push(new Equipment(data));
			});
			return equipments;
		});
	}


	public async getCarById(select: Array<string>, id: number, from: Date, to: Date): Promise<Car> {
		return new Promise((resolve, reject) => {

			let query = GraphqlQueryBuilder.createGplGuery(
				'rent.getCarById',
				select,
				{id: id}
			);

			query = query.replace('@rentPriceFrom', '"' + this.dateToString(from) + '"');
			query = query.replace('@rentPriceTo', '"' + this.dateToString(to) + '"');

			return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query})
				.then((carData: object) => {
					const car = new Car(carData['data']['rent']['getCarById']);
					resolve(car);
				}).catch(err => {
					reject(err);
				})
		});
	}



	public async readAvailableRentCar(
		select: Array<string>,
		from?: Date,
		to?: Date,
		limit?: number,
		offset?: number,
		seats?: number,
		types?: Array<string>,
		equipmentIds?: Array<number>,
		locationId?: number,
		gearbox?: string
	): Promise<CarList> {
		return new Promise((resolve, reject) => {

			select.map((value, key) => {
				select[key] = select[key].replace(
					'rentPrice {',
					'rentPrice(from: "' + this.dateToString(from) + '", to: "' + this.dateToString(to) + '") {'
				);
			});


			return this.readRentCar('rent.readAvailableRentCar', select, from, to, limit, offset, seats, types, equipmentIds, locationId, gearbox)
				.then((data: object) => {

					let cars = [];
					if (data['data']['rent']['readAvailableRentCar']['cars'] !== undefined) {
						data['data']['rent']['readAvailableRentCar']['cars'].map((item) => {
							cars.push(new Car(item))
						});
					}

					const carList = new CarList(data['data']['rent']['readAvailableRentCar']['totalCount'], cars);
					resolve(carList);
				}).catch(err => {
					reject(err);
				})
		});
	}


	public async readUnavailableRentCar(
		select: Array<string>,
		from?: Date,
		to?: Date,
		limit?: number,
		offset?: number,
		seats?: number,
		types?: Array<string>,
		equipmentIds?: Array<number>,
		locationId?: number,
		gearbox?: string
	): Promise<CarList> {
		return new Promise((resolve, reject) => {
			return this.readRentCar('rent.readUnavailableRentCar', select, from, to, limit, offset, seats, types, equipmentIds, locationId, gearbox)
				.then((data: object) => {

					let cars = [];
					if (data['data']['rent']['readUnavailableRentCar']['cars'] !== undefined) {
						data['data']['rent']['readUnavailableRentCar']['cars'].map((item) => {
							cars.push(new Car(item))
						});
					}

					const carList = new CarList(data['data']['rent']['readUnavailableRentCar']['totalCount'], cars);
					resolve(carList);
				}).catch(err => {
					reject(err);
				})
		});

	}



	protected readRentCar(
	  queryName: string,
      select: Array<string>,
      from?: Date,
      to?: Date,
      limit?: number,
      offset?: number,
      seats?: number,
      types?: Array<string>,
	  equipmentIds?: Array<number>,
      locationId?: number,
	  gearbox?: string
	) {
		let args = {};

		if (from !== undefined) {
			args['from'] = this.dateToString(from);
		}

		if (to !== undefined) {
			args['to'] = this.dateToString(to);
		}

		if (limit !== undefined) {
			args['limit'] = limit;
		}

		if (offset !== undefined) {
			args['offset'] = offset;
		}

		if (seats !== undefined) {
			args['seats'] = seats;
		}

		if (types !== undefined && types.length > 0) {
			args['type'] = types;
		}

		if (equipmentIds !== undefined && equipmentIds.length > 0) {
			args['equipmentIds'] = equipmentIds;
		}

		if (locationId !== undefined) {
			args['locationId'] = locationId;
		}

		if (gearbox !== undefined) {
			args['gearbox'] = gearbox;
		}

		const query = GraphqlQueryBuilder.createGplGuery(
			queryName,
			select,
			args
		);

		return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query})
	}

}
