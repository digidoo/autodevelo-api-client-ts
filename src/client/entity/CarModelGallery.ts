import {Entity} from "./Entity";
import {File} from "./File";

export class CarModelGallery extends Entity{

    images?: Array<File>;


    public constructor(data: object) {
        super(data);
        if (data['images'] !== undefined && data['images'] !== null) {
            this.images = [];
            data['images'].forEach(image => this.images.push(new File(image)));
        }
    }

}
