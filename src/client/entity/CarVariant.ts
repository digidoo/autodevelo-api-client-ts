import {Entity} from "./Entity";
import {CarEquipmentLevel} from "./CarEquipmentLevel";
import {CarEngine} from "./CarEngine";
import {CarModel} from "./CarModel";
import {CarModelYear} from "./CarModelYear";
import {CarBodywork} from "./CarBodywork";
import {CarGearbox} from "./CarGearbox";

export class CarVariant extends Entity {

	engine?: CarEngine;
	model?: CarModel;
	modelYear?: CarModelYear;
	equipmentLevel?: CarEquipmentLevel;
	bodywork?: CarBodywork;
	gearbox?: CarGearbox;
	name?: string;


	public constructor(data: object) {
		super(data);

		this.model = this.isNullOrUndefined(data['model'])
			? undefined : new CarModel(data['model']);

		this.engine = this.isNullOrUndefined(data['engine'])
			? undefined : new CarEngine(data['engine']);

		this.modelYear = this.isNullOrUndefined(data['modelYear'])
			? undefined : new CarModelYear(data['modelYear']);

		this.bodywork = this.isNullOrUndefined(data['bodywork'])
			? undefined : new CarBodywork(data['bodywork']);

		this.equipmentLevel = this.isNullOrUndefined(data['equipmentLevel'])
			? undefined : new CarEquipmentLevel(data['equipmentLevel']);

		this.gearbox = this.isNullOrUndefined(data['gearbox'])
			? undefined : new CarGearbox(data['gearbox']);

		this.setProperty('name', data);
	}


}
