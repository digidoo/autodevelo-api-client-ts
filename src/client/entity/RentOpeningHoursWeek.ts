import {Entity} from "./Entity";
import {DayOpeningHours} from "./DayOpeningHours";

export class RentOpeningHoursWeek extends Entity {

	monday: DayOpeningHours;
	tuesday: DayOpeningHours;
	wednesday: DayOpeningHours;
	thursday: DayOpeningHours;
	friday: DayOpeningHours;
	saturday: DayOpeningHours;
	sunday: DayOpeningHours;

	public constructor(data: object) {
		super(data);
		this.monday = data['monday'] !== undefined ? new DayOpeningHours(data['monday']) : undefined;
		this.tuesday = data['tuesday'] !== undefined ? new DayOpeningHours(data['tuesday']) : undefined;
		this.wednesday = data['wednesday'] !== undefined ? new DayOpeningHours(data['wednesday']) : undefined;
		this.thursday = data['thursday'] !== undefined ? new DayOpeningHours(data['thursday']) : undefined;
		this.friday = data['friday'] !== undefined ? new DayOpeningHours(data['friday']) : undefined;
		this.saturday = data['saturday'] !== undefined ? new DayOpeningHours(data['saturday']) : undefined;
		this.sunday = data['sunday'] !== undefined ? new DayOpeningHours(data['sunday']) : undefined;
	}

}
