import {Entity} from "./Entity";
import {RentService} from "./RentService";

export class RentHasService extends Entity {

	service?: RentService;
	count?: number;

	public constructor(data: {}) {
		super(data);
		this.setProperty('count', data);

		this.service = this.isNullOrUndefined(data['service'])
			? undefined : new RentService(data['service']);
	}

	public getPriceTotal(days: number) : number {
		return this.service.priceDay * this.count * days;
	}

	public getPriceTotalTax(days: number) : number {
		return this.service.priceDayTax * this.count * days;
	}
}
