import {Car} from "./Car";

export class CarList {

	totalCount: number;
	cars: Array<Car>;

	constructor(totalCount: number, cars: Array<Car>) {
		this.totalCount = totalCount;
		this.cars = cars;
	}

}
