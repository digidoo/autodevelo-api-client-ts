import {CarModel} from "./CarModel";
import {Entity} from "./Entity";
import {CarEquipmentLevel} from "./CarEquipmentLevel";
import {CarLocation} from "./CarLocation";
import {CarVariant} from "./CarVariant";
import {EquipmentValue} from "./EquipmentValue";
import {CarModelYear} from "./CarModelYear";
import {TollSticker} from "./TollSticker";
import {RentService} from "./RentService";
import {RentPrice} from "./RentPrice";
import {FittingVariant} from "./FittingVariant";
import {CarModelGallery} from "./CarModelGallery";

export class Car extends Entity {

	public static readonly GEARBOX_MANUAL = 'manual';
	public static readonly GEARBOX_AUTOMATIC = 'automatic';

	_title?: string;
	identification?: string;
	type?: string;
	model?: CarModel;
	modelYear?: CarModelYear;
	equipmentLevel?: CarEquipmentLevel;
	equipmentValues?: Array<EquipmentValue>;
	location?: CarLocation;
	variant?: CarVariant;
	tollStickers?: Array<TollSticker>;
	seats?: number;
	gearbox?: string;
	beds?: number;
	tables?: number;
	year?: number;
	rentServices: Array<RentService>;
	rentPrice?: RentPrice;
	isInService?: string;
	allowOfferedServices?: boolean;
	fittingVariants?: Array<FittingVariant>;
	modelGallery?: CarModelGallery;

	volume?: number;
	engineType?: string;
	power?: string;


	public constructor(data: object) {
		super(data);
		this.setProperty('_title', data);
		this.setProperty('identification', data);
		this.setProperty('type', data);
		this.setProperty('seats', data);
		this.setProperty('beds', data);
		this.setProperty('tables', data);
		this.setProperty('gearbox', data);
		this.setProperty('type', data);
		this.setProperty('year', data);
		this.setProperty('volume', data);
		this.setProperty('power', data);
		this.setProperty('engineType', data);
		this.setBoolProperty('isInService', data);
		this.setBoolProperty('allowOfferedServices', data);




		this.variant = this.isNullOrUndefined(data['variant'])
		? undefined : new CarVariant(data['variant']);

		this.model = this.isNullOrUndefined(data['model'])
			? undefined : new CarModel(data['model']);

		this.modelGallery = this.isNullOrUndefined(data['modelGallery'])
			? undefined : new CarModelGallery(data['modelGallery']);

		this.equipmentLevel = this.isNullOrUndefined(data['equipmentLevel'])
			? undefined : new CarEquipmentLevel(data['equipmentLevel']);

		this.location = this.isNullOrUndefined(data['location'])
			? undefined : new CarLocation(data['location']);

		this.modelYear = this.isNullOrUndefined(data['modelYear'])
			? undefined : new CarModelYear(data['modelYear']);

		this.rentPrice = this.isNullOrUndefined(data['rentPrice'])
			? undefined : new RentPrice(data['rentPrice']);


		if (!this.isNullOrUndefined(data['equipmentValues'])) {
			let equipmentValues = new Array<EquipmentValue>();
			data['equipmentValues'].map((value) => {
				equipmentValues.push(new EquipmentValue(value));
			});
			this.equipmentValues = equipmentValues;
		}


		if (!this.isNullOrUndefined(data['tollStickers'])) {
			this.tollStickers = new Array<TollSticker>();
			data['tollStickers'].map((value) => {
				this.tollStickers.push(new TollSticker(value));
			});
		}

		if (!this.isNullOrUndefined(data['rentServices'])) {
			this.rentServices = new Array<RentService>();
			data['rentServices'].map((value) => {
				this.rentServices.push(new RentService(value));
			});
		}

		if (!this.isNullOrUndefined(data['fittingVariants'])) {
			this.fittingVariants = new Array<FittingVariant>();
			data['fittingVariants'].map((value) => {
				this.fittingVariants.push(new FittingVariant(value));
			});
		}
	}


	public printSeats(noneString: string = '-'): string {
		return this.isNullOrUndefined(this.seats) ? noneString : String(this.seats);
	}


	public printTables(noneString: string = '-'): string {
		return this.isNullOrUndefined(this.tables) ? noneString : String(this.tables);
	}


	public printBeds(noneString: string = '-'): string {
		return this.isNullOrUndefined(this.beds) ? noneString : String(this.beds);
	}


	public printCarEngine(noneString: string = '-'): string {
		if (!this.volume && !this.engineType) return noneString;
		return this.volume + 'ccm ' + this.engineType;
	}


	// needs properties: isInService, rentPrice, location
	public isAvailalbeForRent(): boolean {
		if (this.isInService) return false;
		if (!this.rentPrice.price) return false;
		if (!this.location) return false;
		return true;
	}


}
