import {Entity} from "./Entity";

export class TollStickerType extends Entity {

	name?: string;


	public constructor(data: object) {
		super(data);
		this.setProperty('name', data);
	}

}
