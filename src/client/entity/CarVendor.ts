import {Entity} from "./Entity";

export class CarVendor extends Entity {

	name?: string;

	public constructor(data: object) {
		super(data);
		this.name = data['name'];
	}
}
