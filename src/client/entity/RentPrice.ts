import {Entity} from "./Entity";
import {PriceList} from "./PriceList";

export class RentPrice extends Entity{


	from?: Date;
	to?: Date;
	price?: number;
	priceTax?: number;
	priceDay?: number;
	priceDayTax?: number;
	priceRent?: number;
	priceRentTax?: number;
	priceDeposit?: number;
	priceBail?: number;
	priceDiscount?: number;
	priceDiscountTax?: number;
	priceNoDiscount?: number;
	priceNoDiscountTax?: number;

	km?: number;
	dayKmAvg?: number;
	days?: number;

	priceList?: PriceList;


	constructor(data: object) {
		super(data);
		this.setProperty('from', data);
		this.setProperty('to', data);
		this.setProperty('price', data);
		this.setProperty('priceTax', data);
		this.setProperty('priceDay', data);
		this.setProperty('priceDayTax', data);
		this.setProperty('km', data);
		this.setProperty('dayKmAvg', data);
		this.setProperty('days', data);
		this.setProperty('priceRent', data);
		this.setProperty('priceRentTax', data);
		this.setProperty('priceDeposit', data);
		this.setProperty('priceBail', data);

		this.setProperty('priceDiscount', data);
		this.setProperty('priceDiscountTax', data);
		this.setProperty('priceNoDiscount', data);
		this.setProperty('priceNoDiscountTax', data);

		if (!this.isNullOrUndefined(data['priceList'])) {
			this.priceList = new PriceList(data['priceList']);
		}
	}

	public calculatePercentageDiscount(): number {
		if (!this.priceDiscount) return null;
		if (!this.price) return null;

		return Math.round((this.priceDiscount * 100) / this.price);
	}



}
