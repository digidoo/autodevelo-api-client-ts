import {Entity} from "./Entity";
import {TollStickerType} from "./TollStickerType";

export class TollSticker extends Entity {

	type?: TollStickerType;
	validFrom?: Date;
	validTo?: Date;
	note?: string;

	public constructor(data: object) {
		super(data);
		this.setProperty('validFrom', data);
		this.setProperty('validTo', data);
		this.setProperty('note', data);
		this.isNullOrUndefined(data['type']) ? this.type = undefined : this.type = new TollStickerType(data['type']);
	}

}

