import {Entity} from "./Entity";

export class Country extends Entity {

	name?: string;
	nameLong?: string;
	code?: string;
	codeLong?: string;
	isFavorite?: string;


	public constructor(data: object) {
		super(data);
		this.setProperty('name', data);
		this.setProperty('nameLong', data);
		this.setProperty('code', data);
		this.setProperty('codeLong', data);
		this.setProperty('isFavorite', data);
	}


}
