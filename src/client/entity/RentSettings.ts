import {Entity} from "./Entity";
import {RentOpeningHours} from "./RentOpeningHours";
import {Country} from "./Country";
import {SettingExternalCarRental} from "./SettingExternalCarRental";

export class RentSettings extends Entity {

	openingHours: RentOpeningHours;
	allowedDestinations?: Array<Country>;
	settingExternalCarRental?: SettingExternalCarRental;

	public constructor(data: object) {
		super(data);
		this.openingHours = data['openingHours'] !== undefined ? new RentOpeningHours(data['openingHours']) : undefined;
		this.settingExternalCarRental = data['settingExternalCarRental'] !== undefined ? new SettingExternalCarRental(data['settingExternalCarRental']) : undefined;
		if (data['allowedDestinations'] !== undefined) {
			this.allowedDestinations = this.createAllowedDestinations(data);
		}
	}

	private createAllowedDestinations(data: object): Array<Country> {
		const countries = [];
		data['allowedDestinations'].forEach(countryData => {
			countries.push(new Country(countryData));
		});
		return countries;
	}

}
