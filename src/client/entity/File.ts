import {Entity} from "./Entity";

export class File extends Entity {

	url?: string;
	extension?: string;
	isImage?: boolean;

	public constructor(data: {}) {
		super(data);
		this.setProperty('url', data);
		this.setProperty('extension', data);
		this.setProperty('is_image', data);
	}

}
