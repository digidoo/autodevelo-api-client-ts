import {Entity} from "./Entity";
import {CarFitting} from "./CarFitting";

export class FittingVariant extends Entity {

    fitting?: CarFitting;

    constructor(data: object) {
        super(data);
        this.fitting = data['fitting'] === undefined ? undefined : new CarFitting(data['fitting']);
    }

}
