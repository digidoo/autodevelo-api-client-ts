import {Entity} from "./Entity";

export class CarBodywork extends Entity {

	identification?: string;
	name?: string;

	public constructor(data: object) {
		super(data);

		this.setProperty('name', data);
		this.setProperty('identification', data);
	}

}
