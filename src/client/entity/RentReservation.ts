import {Car} from "./Car";
import {Entity} from "./Entity";
import {CarLocation} from "./CarLocation";
import {Country} from "./Country";
import {Client} from "./Client";
import moment = require("moment");
import {Moment} from "moment";
import {RentHasService} from "./RentHasService";
import {InvoicePayType} from "./InovicePayType";
import {Invoice} from "./Invoice";

export class RentReservation extends Entity {

	number?: string;
	_title?: string;
	confirmed?: string;
	rentFrom?: string;
	rentTo?: string;
	car?: Car;
	freeParking?: boolean;
	travelers?: number;
	daysTotal?: number;
	locationRent?: CarLocation;
	locationReturn?: CarLocation;
	client?: Client;
	destination?: Country;
	services: Array<RentHasService>;
	note?: string;

	rentPrice?: number;
	deposit?: number;
	bail?: number;
	discount?: number;
	taxPercent?: number;

	payType?: InvoicePayType;
	payTypeDeposit?: InvoicePayType;
	payTypeBail?: InvoicePayType;
	invoices: Array<Invoice>;



	constructor(data: object) {
		super(data);
		this.setProperty('_title', data);
		this.setProperty('number', data);
		this.setProperty('confirmed', data);
		this.setProperty('rentFrom', data);
		this.setProperty('rentTo', data);
		this.setProperty('freeParking', data);
		this.setProperty('travelers', data);
		this.setProperty('daysTotal', data);
		this.setProperty('note', data);

		this.setProperty('rentPrice', data);
		this.setProperty('deposit', data);
		this.setProperty('bail', data);
		this.setProperty('discount', data);
		this.setProperty('taxPercent', data);

		this.locationRent = this.isNullOrUndefined(data['locationRent'])
			? undefined : new CarLocation(data['locationRent']);

		this.client = this.isNullOrUndefined(data['client'])
			? undefined : new Client(data['client']);

		this.locationReturn = this.isNullOrUndefined(data['locationReturn'])
			? undefined : new CarLocation(data['locationReturn']);

		this.destination = this.isNullOrUndefined(data['destination'])
			? undefined : new Country(data['destination']);


		this.car = this.isNullOrUndefined(data['car']) ? undefined : new Car(data['car']);

		if (!this.isNullOrUndefined(data['services'])) {
			let services = [];
			data['services'].map((value) => {
				services.push(new RentHasService(value));
			});
			this.services = services;
		}

		if (!this.isNullOrUndefined(data['invoices'])) {
			let invoices = [];
			data['invoices'].map((value) => {
				invoices.push(new Invoice(value));
			});
			this.invoices = invoices;
		}

		this.payType = this.isNullOrUndefined(data['payType'])
			? undefined : new InvoicePayType(data['payType']);


		this.payTypeDeposit = this.isNullOrUndefined(data['payTypeDeposit'])
			? undefined : new InvoicePayType(data['payTypeDeposit']);


		this.payTypeBail = this.isNullOrUndefined(data['payTypeBail'])
			? undefined : new InvoicePayType(data['payTypeBail']);
	}

	public getRentFromDate(): Moment {
		return moment(this.rentFrom);
	}

	public getRentToDate(): Moment {
		return moment(this.rentTo);
	}

	public getRentPrice(): number {
		return this.rentPrice;
	}

	public getRentPriceTax(): number {
		return this.getRentPrice() * ((100 + this.taxPercent) / 100);
	}

	public getDiscount(): number {
		return this.discount;
	}

	public getDiscountTax(): number {
		return this.getDiscount() * ((100 + this.taxPercent) / 100);
	}

	public getDeposit(): number {
		return this.deposit / ((100 + this.taxPercent) / 100);
	}

	public getDepositTax(): number {
		return this.deposit;
	}

	public getBail(): number {
		return this.bail / ((100 + this.taxPercent) / 100);
	}

	public getBailTax(): number {
		return this.bail;
	}

	public getTotalPrice(): number {
		return this.rentPrice - this.discount;
	}

	public getTotalPriceTax(): number {
		return this.getRentPriceTax() - this.getDiscountTax();
	}
}
