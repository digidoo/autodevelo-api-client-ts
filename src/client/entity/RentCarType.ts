import {Entity} from "./Entity";

export class RentCarType extends Entity {

	type?: string;
	minRentPrice?: number;
	maxSeats?: number;
	carCount?: number;


	public constructor(data: object) {
		super(data);
		this.setProperty('type', data);
		this.setProperty('maxSeats', data);
		this.setProperty('minRentPrice', data);
		this.setProperty('carCount', data);
	}
}
