import {Entity} from "./Entity";
import {File} from "./File";

export class RentService extends Entity {

	name?: string;
	priceDay?: number;
	priceDayTax?: number;
	taxPercent?: number;
	taxType?: string;
	count?: number;
	description?: string;
	image?: File;

	public constructor(data: {}) {
		super(data);
		this.setProperty('name', data);
		this.setProperty('priceDay', data);
		this.setProperty('priceDayTax', data);
		this.setProperty('taxPercent', data);
		this.setProperty('taxType', data);
		this.setProperty('count', data);
		this.setProperty('description', data);

		this.image = this.isNullOrUndefined(data['image']) ? undefined : new File(data['image']);
	}

}
