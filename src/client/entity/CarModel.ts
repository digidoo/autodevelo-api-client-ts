import {CarVendor} from "./CarVendor";
import {Entity} from "./Entity";

export class CarModel extends Entity{

	identification?: string;
	name?: string;
	vendor?: CarVendor;


	public constructor(data: object) {
		super(data);
		this.identification = data['identification'];
		this.setProperty('name', data)
		this.vendor = data['vendor'] !== undefined ? new CarVendor(data['vendor']) : undefined;
	}

}
