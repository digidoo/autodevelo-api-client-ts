import {Entity} from "./Entity";

export class PriceList extends Entity {

	hasBail: boolean;
	hasDeposit: boolean;

	public constructor(data: object) {
		super(data);
		this.setBoolProperty('hasBail', data);
		this.setBoolProperty('hasDeposit', data);
	}


}
