import {Entity} from "./Entity";
import {RentOpeningHoursWeek} from "./RentOpeningHoursWeek";
import moment = require("moment");

export class RentOpeningHours extends Entity {

	rent: RentOpeningHoursWeek;
	returnRent: RentOpeningHoursWeek;

	public constructor(data: object) {
		super(data);
		this.rent = data['rent'] !== undefined ? new RentOpeningHoursWeek(data['rent']) : undefined;
		this.returnRent = data['returnRent'] !== undefined ? new RentOpeningHoursWeek(data['returnRent']) : undefined;
	}


	public isOpenInDayForRent(date: Date) {
		const dateMoment = moment(date);
		const dayName = dateMoment.locale('en').format('dddd').toLowerCase();

		return !this.rent[dayName].closed;
	}


	public isOpenInDayForRentReturn(date: Date) {
		const dateMoment = moment(date);
		const dayName = dateMoment.locale('en').format('dddd').toLowerCase();
		return !this.returnRent[dayName].closed;
	}


}
