import {Entity} from "./Entity";

export class DayOpeningHours extends Entity {

	closed: boolean;
	from: string;
	to: string;


	public constructor(data: object) {
		super(data);
		this.setBoolProperty('closed', data);
		this.setProperty('from', data);
		this.setProperty('to', data);
	}

}
