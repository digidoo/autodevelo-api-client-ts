import {Entity} from "./Entity";

export class InvoicePayType extends Entity {

	name?: string;

	public constructor(data: {}) {
		super(data);
		this.setProperty('name', data);
	}

}
