import {Entity} from "./Entity";

export class Equipment extends Entity {

	name?: string;
	description?: string;

	public constructor(data: object) {
		super(data);
		this.setProperty('name', data);
		this.setProperty('description', data);
	}

}
