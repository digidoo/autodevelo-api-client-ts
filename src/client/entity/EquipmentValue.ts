import {Entity} from "./Entity";
import {Equipment} from "./Equipment";
import {StringHelper} from "../../utils/StringHelper";

export class EquipmentValue extends Entity {

	equipment?: Equipment;
	value?: string;

	public constructor(data: object) {
		super(data);
		this.equipment = this.isNullOrUndefined(data['equipment']) ? undefined : new Equipment(data['equipment']);
		this.setProperty('value', data);
	}


	public printtEquipment(equipmentValue: EquipmentValue): string {
		if (StringHelper.isANumber(equipmentValue.value)) {
			return equipmentValue.equipment.name + ' - ' + equipmentValue.value + 'x';
		} else if (!equipmentValue.value) {
			return equipmentValue.equipment.name;
		} else {
			return equipmentValue.equipment.name + ' - ' + equipmentValue.value;
		}
	}

}
