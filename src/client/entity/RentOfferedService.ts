import {Entity} from "./Entity";

export class RentOfferedService extends Entity {

	id?: number;
	name?: string;
	description?: string;

	public constructor(data: object) {
		super(data);
		this.setProperty('id', data);
		this.setProperty('name', data);
		this.setProperty('description', data);
	}


}
