import {Entity} from "./Entity";

export class CarFitting extends Entity {

    identification?: string;
    name?: string;
    description?: string;
    weight?: number;

    constructor(data: object) {
        super(data);
        this.setProperty('identification', data);
        this.setProperty('name', data);
        this.setProperty('description', data);
        this.setProperty('weight', data);
    }

}
