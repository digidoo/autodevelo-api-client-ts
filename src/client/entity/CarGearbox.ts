import {Entity} from "./Entity";

export class CarGearbox extends Entity {

	name?: string;
	identification?: string;
	type?: string;
	gears?: number;


	public constructor(data: object) {
		super(data);
		this.setProperty('name', data);
		this.setProperty('identification', data);
		this.setProperty('type', data);
		this.setProperty('gears', data);
	}

}
