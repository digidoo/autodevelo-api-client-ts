import {Entity} from "./Entity";

export class Client extends Entity {

	name?: string;
	company?: string;
	surname?: string;
	street?: string;
	number?: string;
	zip?: string;
	city?: string;
	email?: string;
	phone?: string;
	ic?: string;

	nameDelivery?: string;
	streetDelivery?: string;
	cityDelivery?: string;
	zipDelivery?: string;

	constructor(data: {}) {
		super(data);
		this.setProperty('company', data);
		this.setProperty('name', data);
		this.setProperty('surname', data);
		this.setProperty('street', data);
		this.setProperty('number', data);
		this.setProperty('zip', data);
		this.setProperty('city', data);
		this.setProperty('email', data);
		this.setProperty('phone', data);
		this.setProperty('ic', data);

		this.setProperty('nameDelivery', data);
		this.setProperty('streetDelivery', data);
		this.setProperty('cityDelivery', data);
		this.setProperty('zipDelivery', data);
	}


}
