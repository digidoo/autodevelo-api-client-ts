import {Entity} from "./Entity";

export class CarEngine extends Entity {

	name?: string;
	powerKw?: number;

	public constructor(data: object) {
		super(data);
		this.setProperty('name', data);
		this.setProperty('powerKw', data);
	}

}
