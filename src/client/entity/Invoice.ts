import {Entity} from "./Entity";

export class Invoice extends Entity {

	type?: string;
	number?: string;

	constructor(data: {}) {
		super(data);
		this.setProperty('type', data);
		this.setProperty('number', data);
	}

}
