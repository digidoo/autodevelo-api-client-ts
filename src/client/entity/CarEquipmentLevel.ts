import {Entity} from "./Entity";

export class CarEquipmentLevel extends Entity {

	name?: string;
	identification?: string;

	public constructor(data: object) {
		super(data);
		this.identification = data['identification'];
		this.name = data['name'];
	}

}
