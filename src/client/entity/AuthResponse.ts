
export class AuthResponse {

	accessToken: string;
	refreshToken: string;
	tokenType: string;
	expiresIn: number;

	public constructor(accessToken: string, refreshToken: string, tokenType: string, expiresIn: number) {
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.tokenType = tokenType;
		this.expiresIn = expiresIn;
	}

}
