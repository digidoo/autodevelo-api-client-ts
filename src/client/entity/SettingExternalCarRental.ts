import {Entity} from "./Entity";

export class SettingExternalCarRental extends Entity {

    airConditioningEquipmentIds?: Array<number>;
    airConditioningFittingIds?: Array<number>;

    public constructor(data: object) {
        super(data);
        this.airConditioningEquipmentIds = data['airConditioningEquipmentIds'] !== undefined ? data['airConditioningEquipmentIds'] : undefined;
        this.airConditioningFittingIds = data['airConditioningFittingIds'] !== undefined ? data['airConditioningFittingIds'] : undefined;
    }

}
