export class Entity {

	id?: number;

	public constructor(data: object) {
		this.id = data['id'];
	}

	protected setProperty(name: string, data: object, propertyName?: string) {
		if (propertyName === undefined) {
			propertyName = name;
		}


		if (this.isNullOrUndefined(data[name])) {
			this[propertyName] = undefined;
		} else {
			this[propertyName] = data[name];
		}
	}


	protected setBoolProperty(name: string, data: object) {
		if (this.isNullOrUndefined(data[name])) {
			this[name] = undefined;
			return;
		}

		if (typeof data[name] === 'string') {
			this[name] = data[name] === 'true';
		} else {
			this[name] = data[name];
		}
	}


	protected isNullOrUndefined(value): boolean {
		return value === undefined || value === null;
	}

}
