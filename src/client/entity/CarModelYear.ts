import {Entity} from "./Entity";
import {CarVendor} from "./CarVendor";

export class CarModelYear extends Entity {

	vendor?: CarVendor;
	identification?: string;
	name?: string;

	public constructor(data: object) {
		super(data);
		this.setProperty('identification', data);
		this.setProperty('name', data);

		this.vendor = this.isNullOrUndefined(data['vendor']) ? undefined : new CarVendor(data['vendor']);
	}


}
