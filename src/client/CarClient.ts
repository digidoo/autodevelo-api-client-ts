import {BaseClient} from "./BaseClient";
import {Car} from "./entity/Car";
import {GraphqlQueryBuilder} from "../Graphql/GraphqlQueryBuilder";

export class CarClient extends BaseClient {

	public async getCarById(select: Array<string>, id: number): Promise<Car> {
		return new Promise((resolve, reject) => {

			const query = GraphqlQueryBuilder.createGplGuery(
				'car.getById',
				select,
				{id: id}
			);

			return this.getConnection().sendAuthorizationRequest(BaseClient.API_GRAPHQL, {query: query})
				.then((carData: object) => {
					const car = new Car(carData['data']['car']['getById']);
					resolve(car);
				}).catch(err => {
					reject(err);
				})
		});
	}


}
