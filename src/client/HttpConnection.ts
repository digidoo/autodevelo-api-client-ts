import {IConnection} from "./IConnection";
import {AuthResponse} from "./entity/AuthResponse";
import * as download from 'downloadjs';
import {Utils} from "../utils/Utils";

// import {FormData} from 'form-data';
var FormData = require('form-data');
const fetch = require("node-fetch");


export class HttpConnection implements IConnection {


	private clientId: number;
	private clientSecret: string;
	private url: string;
	private _accessToken;
	private _refreshToken;
	private _tokenType;
	private readonly URL_ACCESS_TOKEN = '/rest/api/v1/access-token';
	private readonly URL_REFRESH_TOKEN = '/rest/api/v1/refresh-token';
	private static authorizationRequestSend: boolean = false;


	public constructor(url: string, clientId: number, clientSecret: string) {
		this.clientId = clientId;
		this.clientSecret = clientSecret;
		this.url = url;
	}





	send(route: string, message: object): Promise<{}> {
		return new Promise( (resolve, reject) => {
			const formData = this.getFormData(message);

			fetch(this.url + route,
				{
					method: 'POST',
					body: formData,
				}
			).then(function (response) {
				response.text().then(function (text) {
					if (response.status !== 200) reject(text);
					try {
						const jsonText = JSON.parse(text);
						resolve(jsonText);
					} catch (err) {
						console.error(err + 'text to parse: ' + text)
						reject(err + 'text to parse: ' + text);
					}
				}).catch(err => {
					reject(err);
				})
			}).catch(err => {
				reject(err);
			});
		});
	}


	private getFormData(object: object) {
		const formData = new FormData();
		Object.keys(object).forEach(key => formData.append(key, object[key]));
		return formData;
	}


	async authorizedDownload(route: string, message: object) {
		message['authorization'] = await this.getAuthorization();

		const formData = this.getFormData(message);
		return this.sendPostRequest(route, formData);
	};


	/**
	 * @param {string} route
	 * @param {object} message
	 * @param {number} attempt - internal, from outside pass ever 0
	 * @returns {Promise<{}>}
	 */
	sendAuthorizationRequest(route: string, message: object, attempt: number = 0): Promise<{}> {
		return new Promise( async (resolve, reject) => {
			message['authorization'] = await this.getAuthorization();

			const formData = this.getFormData(message);

			this.sendPostRequest(route, formData).then((response) => {
				response.text().then((text) => {

					if (response.status === 403) {
						if (attempt > 0) {
							reject(response);
						} else {
							this.clearAccessToken();
							this.sendAuthorizationRequest(route, message, attempt + 1)
								.then((data) => {
									resolve(data);
							}).catch((data) => {
								reject(data);
							})
						}
					} else {
						try {
							const responseData = JSON.parse(text);
							if (responseData['errors'] !== undefined) {
								reject(responseData);
							}
							resolve(responseData);
						} catch (err) {
							console.error(err + 'text to parse: ' + text)
							reject(text);
						}
					}
				}).catch(err => {
					reject(err);
				})
			}).catch(err => {
				reject(err);
			});
		});
	}


	protected async sendPostRequest(route: string, formData) {
		return fetch(this.url + route,
			{
				method: 'POST',
				body: formData,
		});
	}



	protected async getAuthorization(): Promise<string> {
		return new Promise(async (resolve, reject) => {

			if (this._accessToken === undefined) {

				if (HttpConnection.authorizationRequestSend) {
					while (HttpConnection.authorizationRequestSend) {
						console.log('waiting for authorization');
						await Utils.sleep(100);
					}
				}
			}

			if (this._accessToken === undefined) {
				HttpConnection.authorizationRequestSend = true;
				this.authorization(this.clientId, this.clientSecret)
					.then((response: AuthResponse) => {
						this._accessToken = response.accessToken;
						this._refreshToken = response.refreshToken;
						this._tokenType = response.tokenType;
						HttpConnection.authorizationRequestSend = false;
						resolve(this._tokenType + ' ' + this._accessToken);
					}).catch((err) => {
						HttpConnection.authorizationRequestSend = false;
						reject(err);
					});
			} else {
				resolve(this._tokenType + ' ' + this._accessToken);
			}
		});
	}



	protected async authorization(clientId: number, clientSecret: string): Promise<AuthResponse> {
		return new Promise((resolve, reject) => {
				this.send(this.URL_ACCESS_TOKEN, {
						grant_type: 'client_credentials',
						client_id: clientId,
						client_secret: clientSecret,
					}).then((data) => {
					resolve(new AuthResponse(
						data['access_token'],
						data['refresh_token'],
						data['token_type'],
						data['expires_in']
					))
				}).catch((data) => {
					reject(data);
				});
			}
		)
	}


	protected async refreshToken(clientId: number, refreshToken: string) {
		return new Promise((resolve, reject) => {
				this.send(this.URL_REFRESH_TOKEN, {
					grant_type: 'refresh_token',
					refresh_token: this._refreshToken,
					client_id: this.clientId,
				}).then((data) => {
					resolve(new AuthResponse(
						data['access_token'],
						data['refresh_token'],
						data['token_type'],
						data['expires_in']
					))
				}).catch((data) => {
					reject(data);
				});
			}
		)
	}


	private clearAccessToken = () => {
		this._accessToken = undefined;
	}

}
