export class GraphqlQueryBuilder {


	public static createGplGuery(queryName: string, select: string[], args: object): string {
		let query = 'query {';
		let count = 0;

		queryName.split('.').map((value) => {
			query += ' ' + value + ' {';
			count++;
		});


		if (Object.keys(args).length > 0) {
			query = query.substr(0, query.length -1);
			query += '(';

			for (let name in args) {
				let value = args[name];
				if (value instanceof Date) {
					value = this.dateToString(value);
				}

				if (Array.isArray(value)) {
					value = this.arrayToString(value);
					query += name + ': ' + value + ",";
				} else if (typeof value === 'number' || typeof value === 'boolean') {
					query += name + ': ' + value + ",";
				} else {
					query += name + ': \"' + value + "\",";
				}


			}

			query = query.substr(0, query.length -1); // remove last comma


			query += ')';
		}


		query += '{'; // start of select
		select.map((value) => {
			let selectCounter = 0;
			value.split('.').map((value) => {

				selectCounter++;
			});
			value.split('.');
			query += value + ',';
		});

		query = query.substr(0, query.length -1); // remove last comma
		query += '}'; // end of select

		for (let i = 1; i < count; i++) {
			query += '}';
		}


		query += '}';
		return query;
	}


	public static arrayToString(value) {
		let result = '';
		value.map((value) => {
			if (typeof value === 'string') {
				result += '"' + value + '",';
			} else {
				result += '' + value + ',';
			}
		});
		result = result.substring(0, result.length -1);

		return "[" + result + "]";
	}


	public static dateToString(value: Date): string {
		let month = String(value.getMonth());
		let day = String(value.getDate());

		if (month.length === 1) {
			month = "0" + month;
		}

		if (day.length === 1) {
			day = "0" + day;
		}

		return value.getFullYear() + '-' + month + '-' + day;
	}

}
