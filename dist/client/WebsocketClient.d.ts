import { AuthResponse } from "./entity/AuthResponse";
export declare abstract class WebsocketClient {
    private ws?;
    private _accessToken;
    private _refreshToken;
    private clientId;
    private clientSecret;
    private wsUrl;
    constructor(wsUrl: string, clientId: number, clientSecret: string);
    protected send(route: string, message: object): Promise<{}>;
    protected sendAuthorizationRequest(route: string, query: string): Promise<{}>;
    protected authorization(clientId: number, clientSecret: string): Promise<AuthResponse>;
    protected refreshToken(clientId: number, refreshToken: string): Promise<{}>;
    protected getConnection(): WebSocket;
}
