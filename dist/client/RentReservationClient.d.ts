import { BaseClient } from "./BaseClient";
import { RentReservation } from "./entity/RentReservation";
import { ReservationServiceInput } from "./input/ReservationServiceInput";
export declare class RentReservationClient extends BaseClient {
    readReservation(select: string, id: number): Promise<RentReservation>;
    readConfirmedReservations(select: Array<string>, carId: number, from: Date, to: Date): Promise<Array<RentReservation>>;
    readUnconfirmedReservations(select: Array<string>, carId: number, from: Date, to: Date): Promise<Array<RentReservation>>;
    downloadTocPdf(reserveId: number): Promise<any>;
    downloadReservePdf(reserveId: number): Promise<any>;
    downloadInvoicePdf(reserveId: number, invoiceId: number): Promise<any>;
    createReservation(select: string, carId: number, services: Array<ReservationServiceInput>, destinationCountryId: number, locationRentId: number, locationReturnId: number, travelersCount: number, freeParking: boolean, rentFrom: Date, rentTo: Date, clientCountryId: number, clientCompany: string, clientName: string, clientSurname: string, clientStreet: string, clientNumber: string, clientCity: string, clientZip: string, clientEmail: string, clientPhone: string, payTypeId: number, payTypeDepositId: number, payTypeBailId: number, isCarDelivery: boolean, isCarTakeover: boolean, clientIc?: string, note?: string, clientNameDelivery?: string, clientStreetDelivery?: string, clientCityDelivery?: string, clientZipDelivery?: string): Promise<RentReservation>;
    protected readReservations(select: Array<string>, carId: number, from: Date, to: Date, confirmed: boolean): Promise<Array<RentReservation>>;
    protected createRentCarsFromData(data: object): Array<RentReservation>;
}
