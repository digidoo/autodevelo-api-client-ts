"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AuthResponse_1 = require("./entity/AuthResponse");
class WebsocketClient {
    // @todo ukladat do nejakoho sessionStorage? nebo to nema smysl? kdyz nekdo zmackne f5 tak by se zbytecne delal novy access token
    constructor(wsUrl, clientId, clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.wsUrl = wsUrl;
        this._accessToken = null;
        this._refreshToken = null;
    }
    send(route, message) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const data = [
                    7,
                    route,
                    message
                ];
                const connection = this.getConnection();
                connection.onmessage = function (msg) {
                    const data = JSON.parse(msg.data);
                    if (data[1] === route) {
                        if (data[2]['errors'] !== undefined) {
                            reject(data[2]['errors']);
                        }
                        resolve(data[2]);
                    }
                };
                connection.onerror = function (err) {
                    reject(err);
                };
                connection.send(JSON.stringify(data));
            });
        });
    }
    sendAuthorizationRequest(route, query) {
        return __awaiter(this, void 0, void 0, function* () {
            let message = { "query": query };
            if (this._accessToken === null) {
                let authResponse = yield this.authorization(this.clientId, this.clientSecret);
                if (authResponse.accessToken === undefined) {
                    throw new Error('Error'); // @todo vyhodit pres reject
                }
                // @todo dodelat refreshToken - cekat az server vrati odpoved ze vyprsela platnos a vygenerovat novy
                this._accessToken = authResponse.accessToken;
                this._refreshToken = authResponse.refreshToken;
            }
            message['headers'] = {
                'Authorization': {
                    access_token: 'Bearer ' + this._accessToken,
                }
            };
            return this.send(route, message);
        });
    }
    authorization(clientId, clientSecret) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.getConnection().onopen = () => {
                    this.send('/graphql/auth', {
                        "body": {
                            grant_type: 'client_credentials',
                            client_id: clientId,
                            client_secret: clientSecret,
                        }
                    }).then((data) => {
                        resolve(new AuthResponse_1.AuthResponse(data['access_token'], data['refresh_token'], data['token_type'], data['expires_in']));
                    }).catch((data) => {
                        reject(data);
                    });
                };
            });
        });
    }
    refreshToken(clientId, refreshToken) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.send('/graphql/auth', {
                grant_type: 'refresh_token',
                refresh_token: refreshToken,
                client_id: clientId,
            });
        });
    }
    getConnection() {
        if (this.ws === undefined) {
            this.ws = new WebSocket(this.wsUrl);
        }
        return this.ws;
    }
}
exports.WebsocketClient = WebsocketClient;
