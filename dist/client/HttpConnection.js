"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const AuthResponse_1 = require("./entity/AuthResponse");
const Utils_1 = require("../utils/Utils");
// import {FormData} from 'form-data';
var FormData = require('form-data');
const fetch = require("node-fetch");
class HttpConnection {
    constructor(url, clientId, clientSecret) {
        this.URL_ACCESS_TOKEN = '/rest/api/v1/access-token';
        this.URL_REFRESH_TOKEN = '/rest/api/v1/refresh-token';
        this.clearAccessToken = () => {
            this._accessToken = undefined;
        };
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.url = url;
    }
    send(route, message) {
        return new Promise((resolve, reject) => {
            const formData = this.getFormData(message);
            fetch(this.url + route, {
                method: 'POST',
                body: formData,
            }).then(function (response) {
                response.text().then(function (text) {
                    if (response.status !== 200)
                        reject(text);
                    try {
                        const jsonText = JSON.parse(text);
                        resolve(jsonText);
                    }
                    catch (err) {
                        console.error(err + 'text to parse: ' + text);
                        reject(err + 'text to parse: ' + text);
                    }
                }).catch(err => {
                    reject(err);
                });
            }).catch(err => {
                reject(err);
            });
        });
    }
    getFormData(object) {
        const formData = new FormData();
        Object.keys(object).forEach(key => formData.append(key, object[key]));
        return formData;
    }
    authorizedDownload(route, message) {
        return __awaiter(this, void 0, void 0, function* () {
            message['authorization'] = yield this.getAuthorization();
            const formData = this.getFormData(message);
            return this.sendPostRequest(route, formData);
        });
    }
    ;
    /**
     * @param {string} route
     * @param {object} message
     * @param {number} attempt - internal, from outside pass ever 0
     * @returns {Promise<{}>}
     */
    sendAuthorizationRequest(route, message, attempt = 0) {
        return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
            message['authorization'] = yield this.getAuthorization();
            const formData = this.getFormData(message);
            this.sendPostRequest(route, formData).then((response) => {
                response.text().then((text) => {
                    if (response.status === 403) {
                        if (attempt > 0) {
                            reject(response);
                        }
                        else {
                            this.clearAccessToken();
                            this.sendAuthorizationRequest(route, message, attempt + 1)
                                .then((data) => {
                                resolve(data);
                            }).catch((data) => {
                                reject(data);
                            });
                        }
                    }
                    else {
                        try {
                            const responseData = JSON.parse(text);
                            if (responseData['errors'] !== undefined) {
                                reject(responseData);
                            }
                            resolve(responseData);
                        }
                        catch (err) {
                            console.error(err + 'text to parse: ' + text);
                            reject(text);
                        }
                    }
                }).catch(err => {
                    reject(err);
                });
            }).catch(err => {
                reject(err);
            });
        }));
    }
    sendPostRequest(route, formData) {
        return __awaiter(this, void 0, void 0, function* () {
            return fetch(this.url + route, {
                method: 'POST',
                body: formData,
            });
        });
    }
    getAuthorization() {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
                if (this._accessToken === undefined) {
                    if (HttpConnection.authorizationRequestSend) {
                        while (HttpConnection.authorizationRequestSend) {
                            console.log('waiting for authorization');
                            yield Utils_1.Utils.sleep(100);
                        }
                    }
                }
                if (this._accessToken === undefined) {
                    HttpConnection.authorizationRequestSend = true;
                    this.authorization(this.clientId, this.clientSecret)
                        .then((response) => {
                        this._accessToken = response.accessToken;
                        this._refreshToken = response.refreshToken;
                        this._tokenType = response.tokenType;
                        HttpConnection.authorizationRequestSend = false;
                        resolve(this._tokenType + ' ' + this._accessToken);
                    }).catch((err) => {
                        HttpConnection.authorizationRequestSend = false;
                        reject(err);
                    });
                }
                else {
                    resolve(this._tokenType + ' ' + this._accessToken);
                }
            }));
        });
    }
    authorization(clientId, clientSecret) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.send(this.URL_ACCESS_TOKEN, {
                    grant_type: 'client_credentials',
                    client_id: clientId,
                    client_secret: clientSecret,
                }).then((data) => {
                    resolve(new AuthResponse_1.AuthResponse(data['access_token'], data['refresh_token'], data['token_type'], data['expires_in']));
                }).catch((data) => {
                    reject(data);
                });
            });
        });
    }
    refreshToken(clientId, refreshToken) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                this.send(this.URL_REFRESH_TOKEN, {
                    grant_type: 'refresh_token',
                    refresh_token: this._refreshToken,
                    client_id: this.clientId,
                }).then((data) => {
                    resolve(new AuthResponse_1.AuthResponse(data['access_token'], data['refresh_token'], data['token_type'], data['expires_in']));
                }).catch((data) => {
                    reject(data);
                });
            });
        });
    }
}
exports.HttpConnection = HttpConnection;
HttpConnection.authorizationRequestSend = false;
