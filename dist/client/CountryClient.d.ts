import { BaseClient } from "./BaseClient";
import { Country } from "./entity";
export declare class CountryClient extends BaseClient {
    getAll(select: string): Promise<Array<Country>>;
}
