"use strict";
// created from 'create-ts-index'
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./entity"));
__export(require("./input"));
__export(require("./BaseClient"));
__export(require("./CarClient"));
__export(require("./CountryClient"));
__export(require("./HttpConnection"));
__export(require("./RentClient"));
__export(require("./RentOfferedServiceClient"));
__export(require("./RentReservationClient"));
__export(require("./WsConnection"));
