export interface IConnection {
    send(route: string, message: object): Promise<{}>;
    sendAuthorizationRequest(route: string, message: object): Promise<{}>;
    authorizedDownload(route: string, message: object): any;
}
