import { BaseClient } from "./BaseClient";
import { RentOfferedService } from "./entity";
export declare class RentOfferedServiceClient extends BaseClient {
    getAll(select: string): Promise<Array<RentOfferedService>>;
}
