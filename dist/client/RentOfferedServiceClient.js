"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const BaseClient_1 = require("./BaseClient");
const entity_1 = require("./entity");
class RentOfferedServiceClient extends BaseClient_1.BaseClient {
    getAll(select) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const query = 'query { rent { readRentOfferedService { ' + select + ' } } }';
                return this.getConnection().sendAuthorizationRequest(BaseClient_1.BaseClient.API_GRAPHQL, { query: query })
                    .then((data) => {
                    let offeredServices = new Array();
                    data['data']['rent']['readRentOfferedService'].map(data => {
                        offeredServices.push(new entity_1.RentOfferedService(data));
                    });
                    resolve(offeredServices);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
}
exports.RentOfferedServiceClient = RentOfferedServiceClient;
