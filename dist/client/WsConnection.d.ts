import { AuthResponse } from "./entity/AuthResponse";
import { IConnection } from "./IConnection";
export declare abstract class WsConnection implements IConnection {
    private ws?;
    private _accessToken;
    private _refreshToken;
    private clientId;
    private clientSecret;
    private wsUrl;
    constructor(wsUrl: string, clientId: number, clientSecret: string);
    send(route: string, message: object): Promise<{}>;
    authorizedDownload(route: string, message: object): void;
    sendAuthorizationRequest(route: string, query: object): Promise<{}>;
    protected authorization(clientId: number, clientSecret: string): Promise<AuthResponse>;
    protected refreshToken(clientId: number, refreshToken: string): Promise<{}>;
    protected getConnection(): WebSocket;
}
