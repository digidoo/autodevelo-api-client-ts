import { BaseClient } from "./BaseClient";
import { Car } from "./entity/Car";
export declare class CarClient extends BaseClient {
    getCarById(select: Array<string>, id: number): Promise<Car>;
}
