"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const File_1 = require("./File");
class CarModelGallery extends Entity_1.Entity {
    constructor(data) {
        super(data);
        if (data['images'] !== undefined && data['images'] !== null) {
            this.images = [];
            data['images'].forEach(image => this.images.push(new File_1.File(image)));
        }
    }
}
exports.CarModelGallery = CarModelGallery;
