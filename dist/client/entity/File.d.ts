import { Entity } from "./Entity";
export declare class File extends Entity {
    url?: string;
    extension?: string;
    isImage?: boolean;
    constructor(data: {});
}
