"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class SettingExternalCarRental extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.airConditioningEquipmentIds = data['airConditioningEquipmentIds'] !== undefined ? data['airConditioningEquipmentIds'] : undefined;
        this.airConditioningFittingIds = data['airConditioningFittingIds'] !== undefined ? data['airConditioningFittingIds'] : undefined;
    }
}
exports.SettingExternalCarRental = SettingExternalCarRental;
