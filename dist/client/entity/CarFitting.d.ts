import { Entity } from "./Entity";
export declare class CarFitting extends Entity {
    identification?: string;
    name?: string;
    description?: string;
    weight?: number;
    constructor(data: object);
}
