import { CarVendor } from "./CarVendor";
import { Entity } from "./Entity";
export declare class CarModel extends Entity {
    identification?: string;
    name?: string;
    vendor?: CarVendor;
    constructor(data: object);
}
