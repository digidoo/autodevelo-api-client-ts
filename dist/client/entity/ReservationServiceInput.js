"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ReservationServiceInput {
    constructor(serviceId, count) {
        this.serviceId = serviceId;
        this.count = count;
    }
}
exports.ReservationServiceInput = ReservationServiceInput;
