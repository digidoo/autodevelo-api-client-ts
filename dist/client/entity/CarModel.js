"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CarVendor_1 = require("./CarVendor");
const Entity_1 = require("./Entity");
class CarModel extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.identification = data['identification'];
        this.setProperty('name', data);
        this.vendor = data['vendor'] !== undefined ? new CarVendor_1.CarVendor(data['vendor']) : undefined;
    }
}
exports.CarModel = CarModel;
