"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class RentCarType extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('type', data);
        this.setProperty('maxSeats', data);
        this.setProperty('minRentPrice', data);
    }
}
exports.RentCarType = RentCarType;
