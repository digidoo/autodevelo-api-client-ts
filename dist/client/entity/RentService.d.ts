import { Entity } from "./Entity";
import { File } from "./File";
export declare class RentService extends Entity {
    name?: string;
    priceDay?: number;
    priceDayTax?: number;
    taxPercent?: number;
    taxType?: string;
    count?: number;
    description?: string;
    image?: File;
    constructor(data: {});
}
