"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const RentService_1 = require("./RentService");
class RentHasService extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('count', data);
        this.service = this.isNullOrUndefined(data['service'])
            ? undefined : new RentService_1.RentService(data['service']);
    }
    getPriceTotal(days) {
        return this.service.priceDay * this.count * days;
    }
    getPriceTotalTax(days) {
        return this.service.priceDayTax * this.count * days;
    }
}
exports.RentHasService = RentHasService;
