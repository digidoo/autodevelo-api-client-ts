"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const CarVendor_1 = require("./CarVendor");
class CarModelYear extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('identification', data);
        this.setProperty('name', data);
        this.vendor = this.isNullOrUndefined(data['vendor']) ? undefined : new CarVendor_1.CarVendor(data['vendor']);
    }
}
exports.CarModelYear = CarModelYear;
