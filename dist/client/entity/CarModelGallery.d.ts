import { Entity } from "./Entity";
import { File } from "./File";
export declare class CarModelGallery extends Entity {
    images?: Array<File>;
    constructor(data: object);
}
