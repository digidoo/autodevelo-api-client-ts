"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class RentOfferedService extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('id', data);
        this.setProperty('name', data);
        this.setProperty('description', data);
    }
}
exports.RentOfferedService = RentOfferedService;
