import { Entity } from "./Entity";
export declare class RentCarType extends Entity {
    type?: string;
    minRentPrice?: number;
    maxSeats?: number;
    constructor(data: object);
}
