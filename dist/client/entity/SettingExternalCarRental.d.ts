import { Entity } from "./Entity";
export declare class SettingExternalCarRental extends Entity {
    airConditioningEquipmentIds?: Array<number>;
    airConditioningFittingIds?: Array<number>;
    constructor(data: object);
}
