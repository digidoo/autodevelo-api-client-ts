import { Entity } from "./Entity";
import { Equipment } from "./Equipment";
export declare class EquipmentValue extends Entity {
    equipment?: Equipment;
    value?: string;
    constructor(data: object);
    printtEquipment(equipmentValue: EquipmentValue): string;
}
