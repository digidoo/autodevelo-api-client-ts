"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const RentOpeningHours_1 = require("./RentOpeningHours");
const Country_1 = require("./Country");
const SettingExternalCarRental_1 = require("./SettingExternalCarRental");
class RentSettings extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.openingHours = data['openingHours'] !== undefined ? new RentOpeningHours_1.RentOpeningHours(data['openingHours']) : undefined;
        this.settingExternalCarRental = data['settingExternalCarRental'] !== undefined ? new SettingExternalCarRental_1.SettingExternalCarRental(data['settingExternalCarRental']) : undefined;
        if (data['allowedDestinations'] !== undefined) {
            this.allowedDestinations = this.createAllowedDestinations(data);
        }
    }
    createAllowedDestinations(data) {
        const countries = [];
        data['allowedDestinations'].forEach(countryData => {
            countries.push(new Country_1.Country(countryData));
        });
        return countries;
    }
}
exports.RentSettings = RentSettings;
