"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class AuthResponse {
    constructor(accessToken, refreshToken, tokenType, expiresIn) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
    }
}
exports.AuthResponse = AuthResponse;
