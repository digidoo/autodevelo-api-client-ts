"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Entity {
    constructor(data) {
        this.id = data['id'];
    }
    setProperty(name, data, propertyName) {
        if (propertyName === undefined) {
            propertyName = name;
        }
        if (this.isNullOrUndefined(data[name])) {
            this[propertyName] = undefined;
        }
        else {
            this[propertyName] = data[name];
        }
    }
    setBoolProperty(name, data) {
        if (this.isNullOrUndefined(data[name])) {
            this[name] = undefined;
            return;
        }
        if (typeof data[name] === 'string') {
            this[name] = data[name] === 'true';
        }
        else {
            this[name] = data[name];
        }
    }
    isNullOrUndefined(value) {
        return value === undefined || value === null;
    }
}
exports.Entity = Entity;
