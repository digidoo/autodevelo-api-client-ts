"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class PriceList extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setBoolProperty('hasBail', data);
        this.setBoolProperty('hasDeposit', data);
    }
}
exports.PriceList = PriceList;
