"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CarModel_1 = require("./CarModel");
const Entity_1 = require("./Entity");
const CarEquipmentLevel_1 = require("./CarEquipmentLevel");
const CarLocation_1 = require("./CarLocation");
const CarVariant_1 = require("./CarVariant");
const EquipmentValue_1 = require("./EquipmentValue");
const CarModelYear_1 = require("./CarModelYear");
const TollSticker_1 = require("./TollSticker");
const RentService_1 = require("./RentService");
const RentPrice_1 = require("./RentPrice");
const FittingVariant_1 = require("./FittingVariant");
const CarModelGallery_1 = require("./CarModelGallery");
class Car extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('_title', data);
        this.setProperty('identification', data);
        this.setProperty('type', data);
        this.setProperty('seats', data);
        this.setProperty('beds', data);
        this.setProperty('tables', data);
        this.setProperty('gearbox', data);
        this.setProperty('type', data);
        this.setProperty('year', data);
        this.setProperty('volume', data);
        this.setProperty('power', data);
        this.setProperty('engineType', data);
        this.setBoolProperty('isInService', data);
        this.setBoolProperty('allowOfferedServices', data);
        this.variant = this.isNullOrUndefined(data['variant'])
            ? undefined : new CarVariant_1.CarVariant(data['variant']);
        this.model = this.isNullOrUndefined(data['model'])
            ? undefined : new CarModel_1.CarModel(data['model']);
        this.modelGallery = this.isNullOrUndefined(data['modelGallery'])
            ? undefined : new CarModelGallery_1.CarModelGallery(data['modelGallery']);
        this.equipmentLevel = this.isNullOrUndefined(data['equipmentLevel'])
            ? undefined : new CarEquipmentLevel_1.CarEquipmentLevel(data['equipmentLevel']);
        this.location = this.isNullOrUndefined(data['location'])
            ? undefined : new CarLocation_1.CarLocation(data['location']);
        this.modelYear = this.isNullOrUndefined(data['modelYear'])
            ? undefined : new CarModelYear_1.CarModelYear(data['modelYear']);
        this.rentPrice = this.isNullOrUndefined(data['rentPrice'])
            ? undefined : new RentPrice_1.RentPrice(data['rentPrice']);
        if (!this.isNullOrUndefined(data['equipmentValues'])) {
            let equipmentValues = new Array();
            data['equipmentValues'].map((value) => {
                equipmentValues.push(new EquipmentValue_1.EquipmentValue(value));
            });
            this.equipmentValues = equipmentValues;
        }
        if (!this.isNullOrUndefined(data['tollStickers'])) {
            this.tollStickers = new Array();
            data['tollStickers'].map((value) => {
                this.tollStickers.push(new TollSticker_1.TollSticker(value));
            });
        }
        if (!this.isNullOrUndefined(data['rentServices'])) {
            this.rentServices = new Array();
            data['rentServices'].map((value) => {
                this.rentServices.push(new RentService_1.RentService(value));
            });
        }
        if (!this.isNullOrUndefined(data['fittingVariants'])) {
            this.fittingVariants = new Array();
            data['fittingVariants'].map((value) => {
                this.fittingVariants.push(new FittingVariant_1.FittingVariant(value));
            });
        }
    }
    printSeats(noneString = '-') {
        return this.isNullOrUndefined(this.seats) ? noneString : String(this.seats);
    }
    printTables(noneString = '-') {
        return this.isNullOrUndefined(this.tables) ? noneString : String(this.tables);
    }
    printBeds(noneString = '-') {
        return this.isNullOrUndefined(this.beds) ? noneString : String(this.beds);
    }
    printCarEngine(noneString = '-') {
        if (!this.volume && !this.engineType)
            return noneString;
        return this.volume + 'ccm ' + this.engineType;
    }
    // needs properties: isInService, rentPrice, location
    isAvailalbeForRent() {
        if (this.isInService)
            return false;
        if (!this.rentPrice.price)
            return false;
        if (!this.location)
            return false;
        return true;
    }
}
exports.Car = Car;
Car.GEARBOX_MANUAL = 'manual';
Car.GEARBOX_AUTOMATIC = 'automatic';
