import { Entity } from "./Entity";
import { RentOpeningHours } from "./RentOpeningHours";
import { Country } from "./Country";
import { SettingExternalCarRental } from "./SettingExternalCarRental";
export declare class RentSettings extends Entity {
    openingHours: RentOpeningHours;
    allowedDestinations?: Array<Country>;
    settingExternalCarRental?: SettingExternalCarRental;
    constructor(data: object);
    private createAllowedDestinations;
}
