import { Entity } from "./Entity";
export declare class Client extends Entity {
    name?: string;
    company?: string;
    surname?: string;
    street?: string;
    number?: string;
    zip?: string;
    city?: string;
    email?: string;
    phone?: string;
    ic?: string;
    nameDelivery?: string;
    streetDelivery?: string;
    cityDelivery?: string;
    zipDelivery?: string;
    constructor(data: {});
}
