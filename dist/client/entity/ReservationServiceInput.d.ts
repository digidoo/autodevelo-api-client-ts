export declare class ReservationServiceInput {
    count: number;
    serviceId: number;
    constructor(serviceId: number, count: number);
}
