"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class Invoice extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('type', data);
        this.setProperty('number', data);
    }
}
exports.Invoice = Invoice;
