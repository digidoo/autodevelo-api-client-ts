"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class CarFitting extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('identification', data);
        this.setProperty('name', data);
        this.setProperty('description', data);
        this.setProperty('weight', data);
    }
}
exports.CarFitting = CarFitting;
