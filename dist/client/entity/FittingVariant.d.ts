import { Entity } from "./Entity";
import { CarFitting } from "./CarFitting";
export declare class FittingVariant extends Entity {
    fitting?: CarFitting;
    constructor(data: object);
}
