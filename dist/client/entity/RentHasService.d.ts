import { Entity } from "./Entity";
import { RentService } from "./RentService";
export declare class RentHasService extends Entity {
    service?: RentService;
    count?: number;
    constructor(data: {});
    getPriceTotal(days: number): number;
    getPriceTotalTax(days: number): number;
}
