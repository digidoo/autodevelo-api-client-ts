"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class InvoicePayType extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('name', data);
    }
}
exports.InvoicePayType = InvoicePayType;
