export declare class AuthResponse {
    accessToken: string;
    refreshToken: string;
    tokenType: string;
    expiresIn: number;
    constructor(accessToken: string, refreshToken: string, tokenType: string, expiresIn: number);
}
