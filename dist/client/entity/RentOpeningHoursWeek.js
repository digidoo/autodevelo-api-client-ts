"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const DayOpeningHours_1 = require("./DayOpeningHours");
class RentOpeningHoursWeek extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.monday = data['monday'] !== undefined ? new DayOpeningHours_1.DayOpeningHours(data['monday']) : undefined;
        this.tuesday = data['tuesday'] !== undefined ? new DayOpeningHours_1.DayOpeningHours(data['tuesday']) : undefined;
        this.wednesday = data['wednesday'] !== undefined ? new DayOpeningHours_1.DayOpeningHours(data['wednesday']) : undefined;
        this.thursday = data['thursday'] !== undefined ? new DayOpeningHours_1.DayOpeningHours(data['thursday']) : undefined;
        this.friday = data['friday'] !== undefined ? new DayOpeningHours_1.DayOpeningHours(data['friday']) : undefined;
        this.saturday = data['saturday'] !== undefined ? new DayOpeningHours_1.DayOpeningHours(data['saturday']) : undefined;
        this.sunday = data['sunday'] !== undefined ? new DayOpeningHours_1.DayOpeningHours(data['sunday']) : undefined;
    }
}
exports.RentOpeningHoursWeek = RentOpeningHoursWeek;
