"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const Equipment_1 = require("./Equipment");
const StringHelper_1 = require("../../utils/StringHelper");
class EquipmentValue extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.equipment = this.isNullOrUndefined(data['equipment']) ? undefined : new Equipment_1.Equipment(data['equipment']);
        this.setProperty('value', data);
    }
    printtEquipment(equipmentValue) {
        if (StringHelper_1.StringHelper.isANumber(equipmentValue.value)) {
            return equipmentValue.equipment.name + ' - ' + equipmentValue.value + 'x';
        }
        else if (!equipmentValue.value) {
            return equipmentValue.equipment.name;
        }
        else {
            return equipmentValue.equipment.name + ' - ' + equipmentValue.value;
        }
    }
}
exports.EquipmentValue = EquipmentValue;
