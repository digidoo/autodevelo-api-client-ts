import { Entity } from "./Entity";
export declare class Equipment extends Entity {
    name?: string;
    description?: string;
    constructor(data: object);
}
