import { Entity } from "./Entity";
export declare class RentOfferedService extends Entity {
    id?: number;
    name?: string;
    description?: string;
    constructor(data: object);
}
