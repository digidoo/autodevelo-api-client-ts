import { Entity } from "./Entity";
import { TollStickerType } from "./TollStickerType";
export declare class TollSticker extends Entity {
    type?: TollStickerType;
    validFrom?: Date;
    validTo?: Date;
    note?: string;
    constructor(data: object);
}
