"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const CarFitting_1 = require("./CarFitting");
class FittingVariant extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.fitting = data['fitting'] === undefined ? undefined : new CarFitting_1.CarFitting(data['fitting']);
    }
}
exports.FittingVariant = FittingVariant;
