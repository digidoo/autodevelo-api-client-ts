import { IConnection } from "./IConnection";
import { AuthResponse } from "./entity/AuthResponse";
export declare class HttpConnection implements IConnection {
    private clientId;
    private clientSecret;
    private url;
    private _accessToken;
    private _refreshToken;
    private _tokenType;
    private readonly URL_ACCESS_TOKEN;
    private readonly URL_REFRESH_TOKEN;
    private static authorizationRequestSend;
    constructor(url: string, clientId: number, clientSecret: string);
    send(route: string, message: object): Promise<{}>;
    private getFormData;
    authorizedDownload(route: string, message: object): Promise<any>;
    /**
     * @param {string} route
     * @param {object} message
     * @param {number} attempt - internal, from outside pass ever 0
     * @returns {Promise<{}>}
     */
    sendAuthorizationRequest(route: string, message: object, attempt?: number): Promise<{}>;
    protected sendPostRequest(route: string, formData: any): Promise<any>;
    protected getAuthorization(): Promise<string>;
    protected authorization(clientId: number, clientSecret: string): Promise<AuthResponse>;
    protected refreshToken(clientId: number, refreshToken: string): Promise<unknown>;
    private clearAccessToken;
}
