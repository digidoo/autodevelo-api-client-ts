"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GraphqlQueryBuilder {
    static createGplGuery(queryName, select, args) {
        let query = 'query {';
        let count = 0;
        queryName.split('.').map((value) => {
            query += ' ' + value + ' {';
            count++;
        });
        if (Object.keys(args).length > 0) {
            query = query.substr(0, query.length - 1);
            query += '(';
            for (let name in args) {
                let value = args[name];
                if (value instanceof Date) {
                    value = this.dateToString(value);
                }
                if (Array.isArray(value)) {
                    value = this.arrayToString(value);
                    query += name + ': ' + value + ",";
                }
                else if (typeof value === 'number' || typeof value === 'boolean') {
                    query += name + ': ' + value + ",";
                }
                else {
                    query += name + ': \"' + value + "\",";
                }
            }
            query = query.substr(0, query.length - 1); // remove last comma
            query += ')';
        }
        query += '{'; // start of select
        select.map((value) => {
            let selectCounter = 0;
            value.split('.').map((value) => {
                selectCounter++;
            });
            value.split('.');
            query += value + ',';
        });
        query = query.substr(0, query.length - 1); // remove last comma
        query += '}'; // end of select
        for (let i = 1; i < count; i++) {
            query += '}';
        }
        query += '}';
        return query;
    }
    static arrayToString(value) {
        let result = '';
        value.map((value) => {
            if (typeof value === 'string') {
                result += '"' + value + '",';
            }
            else {
                result += '' + value + ',';
            }
        });
        result = result.substring(0, result.length - 1);
        return "[" + result + "]";
    }
    static dateToString(value) {
        let month = String(value.getMonth());
        let day = String(value.getDate());
        if (month.length === 1) {
            month = "0" + month;
        }
        if (day.length === 1) {
            day = "0" + day;
        }
        return value.getFullYear() + '-' + month + '-' + day;
    }
}
exports.GraphqlQueryBuilder = GraphqlQueryBuilder;
