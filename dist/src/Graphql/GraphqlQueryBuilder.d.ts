export declare class GraphqlQueryBuilder {
    static createGplGuery(queryName: string, select: string[], args: object): string;
    static arrayToString(value: any): string;
    static dateToString(value: Date): string;
}
