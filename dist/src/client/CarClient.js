"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = require("./Client");
const Car_1 = require("./entity/Car");
const GraphqlQueryBuilder_1 = require("../Graphql/GraphqlQueryBuilder");
class CarClient extends Client_1.Client {
    getCarById(select, id) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const query = GraphqlQueryBuilder_1.GraphqlQueryBuilder.createGplGuery('car.getById', select, { id: id });
                return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query })
                    .then((carData) => {
                    const car = new Car_1.Car(carData['data']['car']['getById']);
                    resolve(car);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
}
exports.CarClient = CarClient;
