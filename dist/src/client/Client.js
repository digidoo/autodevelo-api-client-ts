"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const HttpConnection_1 = require("./HttpConnection");
class Client {
    constructor(url, clientId, clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.url = url;
    }
    dateToString(value) {
        let month = String(value.getMonth() + 1);
        let day = String(value.getDate());
        let hours = String(value.getHours());
        let minutes = String(value.getMinutes());
        if (month.length === 1) {
            month = "0" + month;
        }
        if (day.length === 1) {
            day = "0" + day;
        }
        if (hours.length === 1) {
            hours = "0" + hours;
        }
        if (minutes.length === 1) {
            minutes = "0" + minutes;
        }
        return value.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + minutes;
    }
    getConnection() {
        if (this.conneciton === undefined) {
            this.conneciton = new HttpConnection_1.HttpConnection(this.url, this.clientId, this.clientSecret);
        }
        return this.conneciton;
    }
}
exports.Client = Client;
