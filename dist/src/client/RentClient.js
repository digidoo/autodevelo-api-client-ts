"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = require("./Client");
const Car_1 = require("./entity/Car");
const GraphqlQueryBuilder_1 = require("../Graphql/GraphqlQueryBuilder");
const CarList_1 = require("./entity/CarList");
const RentCarType_1 = require("./entity/RentCarType");
const entity_1 = require("./entity");
const RentSettings_1 = require("./entity/RentSettings");
class RentClient extends Client_1.Client {
    readRentSettings(select) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = 'query { rent { getRentSettings { ' + select + ' } } }';
            return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query }).then((data) => {
                return new RentSettings_1.RentSettings(data['data']['rent']['getRentSettings']);
            });
        });
    }
    readAvailableCarLocations(select) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = 'query { rent { readAvailableCarLocations { ' + select + ' } } }';
            return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query }).then((data) => {
                let locations = [];
                data['data']['rent']['readAvailableCarLocations'].map((data) => {
                    locations.push(new entity_1.CarLocation(data));
                });
                return locations;
            });
        });
    }
    readAvailableRentCarTypes(select) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = 'query { rent { readAvailableRentCarTypes { ' + select + ' } } }';
            return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query }).then((data) => {
                let rentCarTypes = [];
                data['data']['rent']['readAvailableRentCarTypes'].map((data) => {
                    rentCarTypes.push(new RentCarType_1.RentCarType(data));
                });
                return rentCarTypes;
            });
        });
    }
    readAvailableRentCarEquipments(select) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = 'query { rent { readAvailableCarEquipments { ' + select + ' } } }';
            return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query }).then((data) => {
                let equipments = [];
                data['data']['rent']['readAvailableCarEquipments'].map((data) => {
                    equipments.push(new entity_1.Equipment(data));
                });
                return equipments;
            });
        });
    }
    getCarById(select, id, from, to) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                let query = GraphqlQueryBuilder_1.GraphqlQueryBuilder.createGplGuery('rent.getCarById', select, { id: id });
                query = query.replace('@rentPriceFrom', '"' + this.dateToString(from) + '"');
                query = query.replace('@rentPriceTo', '"' + this.dateToString(to) + '"');
                return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query })
                    .then((carData) => {
                    const car = new Car_1.Car(carData['data']['rent']['getCarById']);
                    resolve(car);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
    readAvailableRentCar(select, from, to, limit, offset, seats, types, equipmentIds, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                select.map((value, key) => {
                    select[key] = select[key].replace('rentPrice {', 'rentPrice(from: "' + this.dateToString(from) + '", to: "' + this.dateToString(to) + '") {');
                });
                return this.readRentCar('rent.readAvailableRentCar', select, from, to, limit, offset, seats, types, equipmentIds, locationId)
                    .then((data) => {
                    let cars = [];
                    if (data['data']['rent']['readAvailableRentCar']['cars'] !== undefined) {
                        data['data']['rent']['readAvailableRentCar']['cars'].map((item) => {
                            cars.push(new Car_1.Car(item));
                        });
                    }
                    const carList = new CarList_1.CarList(data['data']['rent']['readAvailableRentCar']['totalCount'], cars);
                    resolve(carList);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
    readUnavailableRentCar(select, from, to, limit, offset, seats, types, equipmentIds, locationId) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                return this.readRentCar('rent.readUnavailableRentCar', select, from, to, limit, offset, seats, types, equipmentIds, locationId)
                    .then((data) => {
                    let cars = [];
                    if (data['data']['rent']['readUnavailableRentCar']['cars'] !== undefined) {
                        data['data']['rent']['readUnavailableRentCar']['cars'].map((item) => {
                            cars.push(new Car_1.Car(item));
                        });
                    }
                    const carList = new CarList_1.CarList(data['data']['rent']['readUnavailableRentCar']['totalCount'], cars);
                    resolve(carList);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
    readRentCar(queryName, select, from, to, limit, offset, seats, types, equipmentIds, locationId) {
        let args = {};
        if (from !== undefined) {
            args['from'] = this.dateToString(from);
        }
        if (to !== undefined) {
            args['to'] = this.dateToString(to);
        }
        if (limit !== undefined) {
            args['limit'] = limit;
        }
        if (offset !== undefined) {
            args['offset'] = offset;
        }
        if (seats !== undefined) {
            args['seats'] = seats;
        }
        if (types !== undefined && types.length > 0) {
            args['type'] = types;
        }
        if (equipmentIds !== undefined && equipmentIds.length > 0) {
            args['equipmentIds'] = equipmentIds;
        }
        if (locationId !== undefined) {
            args['locationId'] = locationId;
        }
        const query = GraphqlQueryBuilder_1.GraphqlQueryBuilder.createGplGuery(queryName, select, args);
        return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query });
    }
}
exports.RentClient = RentClient;
