export declare class ReservationServiceInput {
    count: number;
    idService: number;
    priceDay?: number;
    priceDayTax?: number;
    constructor(idService: number, count: number, priceDay?: number, priceDayTax?: number);
}
