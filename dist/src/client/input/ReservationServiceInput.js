"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ReservationServiceInput {
    constructor(idService, count, priceDay, priceDayTax) {
        this.idService = idService;
        this.count = count;
        this.priceDay = priceDay;
        this.priceDayTax = priceDayTax;
    }
}
exports.ReservationServiceInput = ReservationServiceInput;
