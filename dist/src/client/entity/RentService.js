"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const File_1 = require("./File");
class RentService extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('name', data);
        this.setProperty('priceDay', data);
        this.setProperty('priceDayTax', data);
        this.setProperty('taxPercent', data);
        this.setProperty('taxType', data);
        this.setProperty('count', data);
        this.setProperty('description', data);
        this.image = this.isNullOrUndefined(data['image']) ? undefined : new File_1.File(data['image']);
    }
}
exports.RentService = RentService;
