"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class CarVendor extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.name = data['name'];
    }
}
exports.CarVendor = CarVendor;
