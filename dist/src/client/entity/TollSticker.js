"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const TollStickerType_1 = require("./TollStickerType");
class TollSticker extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('validFrom', data);
        this.setProperty('validTo', data);
        this.setProperty('note', data);
        this.isNullOrUndefined(data['type']) ? this.type = undefined : this.type = new TollStickerType_1.TollStickerType(data['type']);
    }
}
exports.TollSticker = TollSticker;
