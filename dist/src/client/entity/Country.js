"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class Country extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('name', data);
        this.setProperty('nameLong', data);
        this.setProperty('code', data);
        this.setProperty('codeLong', data);
    }
}
exports.Country = Country;
