"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const PriceList_1 = require("./PriceList");
class RentPrice extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('from', data);
        this.setProperty('to', data);
        this.setProperty('price', data);
        this.setProperty('priceTax', data);
        this.setProperty('priceDay', data);
        this.setProperty('priceDayTax', data);
        this.setProperty('km', data);
        this.setProperty('dayKmAvg', data);
        this.setProperty('days', data);
        this.setProperty('priceRent', data);
        this.setProperty('priceRentTax', data);
        this.setProperty('priceDeposit', data);
        this.setProperty('priceBail', data);
        this.setProperty('priceDiscount', data);
        this.setProperty('priceDiscountTax', data);
        this.setProperty('priceNoDiscount', data);
        this.setProperty('priceNoDiscountTax', data);
        if (!this.isNullOrUndefined(data['priceList'])) {
            this.priceList = new PriceList_1.PriceList(data['priceList']);
        }
    }
}
exports.RentPrice = RentPrice;
