import { Entity } from "./Entity";
export declare class DayOpeningHours extends Entity {
    closed: boolean;
    from: string;
    to: string;
    constructor(data: object);
}
