"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Car_1 = require("./Car");
const Entity_1 = require("./Entity");
const CarLocation_1 = require("./CarLocation");
const Country_1 = require("./Country");
const Client_1 = require("./Client");
const moment = require("moment");
const RentHasService_1 = require("./RentHasService");
const InovicePayType_1 = require("./InovicePayType");
const Invoice_1 = require("./Invoice");
class RentReservation extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('_title', data);
        this.setProperty('number', data);
        this.setProperty('confirmed', data);
        this.setProperty('rentFrom', data);
        this.setProperty('rentTo', data);
        this.setProperty('freeParking', data);
        this.setProperty('travelers', data);
        this.setProperty('daysTotal', data);
        this.setProperty('note', data);
        this.setProperty('rentPrice', data);
        this.setProperty('deposit', data);
        this.setProperty('bail', data);
        this.setProperty('discount', data);
        this.setProperty('taxPercent', data);
        this.locationRent = this.isNullOrUndefined(data['locationRent'])
            ? undefined : new CarLocation_1.CarLocation(data['locationRent']);
        this.client = this.isNullOrUndefined(data['client'])
            ? undefined : new Client_1.Client(data['client']);
        this.locationReturn = this.isNullOrUndefined(data['locationReturn'])
            ? undefined : new CarLocation_1.CarLocation(data['locationReturn']);
        this.destination = this.isNullOrUndefined(data['destination'])
            ? undefined : new Country_1.Country(data['destination']);
        this.car = this.isNullOrUndefined(data['car']) ? undefined : new Car_1.Car(data['car']);
        if (!this.isNullOrUndefined(data['services'])) {
            let services = [];
            data['services'].map((value) => {
                services.push(new RentHasService_1.RentHasService(value));
            });
            this.services = services;
        }
        if (!this.isNullOrUndefined(data['invoices'])) {
            let invoices = [];
            data['invoices'].map((value) => {
                invoices.push(new Invoice_1.Invoice(value));
            });
            this.invoices = invoices;
        }
        this.payType = this.isNullOrUndefined(data['payType'])
            ? undefined : new InovicePayType_1.InvoicePayType(data['payType']);
        this.payTypeDeposit = this.isNullOrUndefined(data['payTypeDeposit'])
            ? undefined : new InovicePayType_1.InvoicePayType(data['payTypeDeposit']);
        this.payTypeBail = this.isNullOrUndefined(data['payTypeBail'])
            ? undefined : new InovicePayType_1.InvoicePayType(data['payTypeBail']);
    }
    getRentFromDate() {
        return moment(this.rentFrom);
    }
    getRentToDate() {
        return moment(this.rentTo);
    }
    getRentPrice() {
        return this.rentPrice;
    }
    getRentPriceTax() {
        return this.getRentPrice() * ((100 + this.taxPercent) / 100);
    }
    getDiscount() {
        return this.discount;
    }
    getDiscountTax() {
        return this.getDiscount() * ((100 + this.taxPercent) / 100);
    }
    getDeposit() {
        return this.getDeposit() / ((100 + this.taxPercent) / 100);
    }
    getDepositTax() {
        return this.deposit;
    }
    getBail() {
        return this.bail / ((100 + this.taxPercent) / 100);
    }
    getBailTax() {
        return this.bail;
    }
    getTotalPrice() {
        return this.rentPrice - this.discount;
    }
    getTotalPriceTax() {
        return this.getRentPriceTax() - this.getDiscountTax();
    }
}
exports.RentReservation = RentReservation;
