import { Entity } from "./Entity";
import { PriceList } from "./PriceList";
export declare class RentPrice extends Entity {
    from?: Date;
    to?: Date;
    price?: number;
    priceTax?: number;
    priceDay?: number;
    priceDayTax?: number;
    priceRent?: number;
    priceRentTax?: number;
    priceDeposit?: number;
    priceBail?: number;
    priceDiscount?: number;
    priceDiscountTax?: number;
    priceNoDiscount?: number;
    priceNoDiscountTax?: number;
    km?: number;
    dayKmAvg?: number;
    days?: number;
    priceList?: PriceList;
    constructor(data: object);
}
