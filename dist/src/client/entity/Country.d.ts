import { Entity } from "./Entity";
export declare class Country extends Entity {
    name?: string;
    nameLong?: string;
    code?: string;
    codeLong?: string;
    constructor(data: object);
}
