import { Entity } from "./Entity";
import { CarEquipmentLevel } from "./CarEquipmentLevel";
import { CarEngine } from "./CarEngine";
import { CarModel } from "./CarModel";
import { CarModelYear } from "./CarModelYear";
import { CarBodywork } from "./CarBodywork";
import { CarGearbox } from "./CarGearbox";
export declare class CarVariant extends Entity {
    engine?: CarEngine;
    model?: CarModel;
    modelYear?: CarModelYear;
    equipmentLevel?: CarEquipmentLevel;
    bodywork?: CarBodywork;
    gearbox?: CarGearbox;
    name?: string;
    constructor(data: object);
}
