"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const RentOpeningHours_1 = require("./RentOpeningHours");
class RentSettings extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.openingHours = data['openingHours'] !== undefined ? new RentOpeningHours_1.RentOpeningHours(data['openingHours']) : undefined;
    }
}
exports.RentSettings = RentSettings;
