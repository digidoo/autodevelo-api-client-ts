import { Entity } from "./Entity";
import { CarVendor } from "./CarVendor";
export declare class CarModelYear extends Entity {
    vendor?: CarVendor;
    identification?: string;
    name?: string;
    constructor(data: object);
}
