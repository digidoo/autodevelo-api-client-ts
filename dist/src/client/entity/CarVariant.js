"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const CarEquipmentLevel_1 = require("./CarEquipmentLevel");
const CarEngine_1 = require("./CarEngine");
const CarModel_1 = require("./CarModel");
const CarModelYear_1 = require("./CarModelYear");
const CarBodywork_1 = require("./CarBodywork");
const CarGearbox_1 = require("./CarGearbox");
class CarVariant extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.model = this.isNullOrUndefined(data['model'])
            ? undefined : new CarModel_1.CarModel(data['model']);
        this.engine = this.isNullOrUndefined(data['engine'])
            ? undefined : new CarEngine_1.CarEngine(data['engine']);
        this.modelYear = this.isNullOrUndefined(data['modelYear'])
            ? undefined : new CarModelYear_1.CarModelYear(data['modelYear']);
        this.bodywork = this.isNullOrUndefined(data['bodywork'])
            ? undefined : new CarBodywork_1.CarBodywork(data['bodywork']);
        this.equipmentLevel = this.isNullOrUndefined(data['equipmentLevel'])
            ? undefined : new CarEquipmentLevel_1.CarEquipmentLevel(data['equipmentLevel']);
        this.gearbox = this.isNullOrUndefined(data['gearbox'])
            ? undefined : new CarGearbox_1.CarGearbox(data['gearbox']);
        this.setProperty('name', data);
    }
}
exports.CarVariant = CarVariant;
