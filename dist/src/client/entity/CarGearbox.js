"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class CarGearbox extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('name', data);
        this.setProperty('identification', data);
        this.setProperty('type', data);
        this.setProperty('gears', data);
    }
}
exports.CarGearbox = CarGearbox;
