"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class CarEquipmentLevel extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.identification = data['identification'];
        this.name = data['name'];
    }
}
exports.CarEquipmentLevel = CarEquipmentLevel;
