import { Entity } from "./Entity";
export declare class CarEngine extends Entity {
    name?: string;
    powerKw?: number;
    constructor(data: object);
}
