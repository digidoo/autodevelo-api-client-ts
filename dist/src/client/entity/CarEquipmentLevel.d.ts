import { Entity } from "./Entity";
export declare class CarEquipmentLevel extends Entity {
    name?: string;
    identification?: string;
    constructor(data: object);
}
