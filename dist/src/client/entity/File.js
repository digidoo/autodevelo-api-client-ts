"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class File extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('url', data);
        this.setProperty('extension', data);
        this.setProperty('is_image', data);
    }
}
exports.File = File;
