import { Car } from "./Car";
export declare class CarList {
    totalCount: number;
    cars: Array<Car>;
    constructor(totalCount: number, cars: Array<Car>);
}
