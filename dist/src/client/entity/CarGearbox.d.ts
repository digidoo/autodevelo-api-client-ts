import { Entity } from "./Entity";
export declare class CarGearbox extends Entity {
    name?: string;
    identification?: string;
    type?: string;
    gears?: number;
    constructor(data: object);
}
