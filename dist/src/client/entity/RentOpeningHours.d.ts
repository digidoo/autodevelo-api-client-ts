import { Entity } from "./Entity";
import { RentOpeningHoursWeek } from "./RentOpeningHoursWeek";
export declare class RentOpeningHours extends Entity {
    rent: RentOpeningHoursWeek;
    returnRent: RentOpeningHoursWeek;
    constructor(data: object);
    isOpenInDayForRent(date: Date): boolean;
    isOpenInDayForRentReturn(date: Date): boolean;
}
