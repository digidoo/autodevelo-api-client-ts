"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
const RentOpeningHoursWeek_1 = require("./RentOpeningHoursWeek");
const moment = require("moment");
class RentOpeningHours extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.rent = data['rent'] !== undefined ? new RentOpeningHoursWeek_1.RentOpeningHoursWeek(data['rent']) : undefined;
        this.returnRent = data['returnRent'] !== undefined ? new RentOpeningHoursWeek_1.RentOpeningHoursWeek(data['returnRent']) : undefined;
    }
    isOpenInDayForRent(date) {
        const dateMoment = moment(date);
        const dayName = dateMoment.locale('en').format('dddd').toLowerCase();
        return !this.rent[dayName].closed;
    }
    isOpenInDayForRentReturn(date) {
        const dateMoment = moment(date);
        const dayName = dateMoment.locale('en').format('dddd').toLowerCase();
        return !this.returnRent[dayName].closed;
    }
}
exports.RentOpeningHours = RentOpeningHours;
