"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CarList {
    constructor(totalCount, cars) {
        this.totalCount = totalCount;
        this.cars = cars;
    }
}
exports.CarList = CarList;
