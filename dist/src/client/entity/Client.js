"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class Client extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setProperty('company', data);
        this.setProperty('name', data);
        this.setProperty('surname', data);
        this.setProperty('street', data);
        this.setProperty('number', data);
        this.setProperty('zip', data);
        this.setProperty('city', data);
        this.setProperty('email', data);
        this.setProperty('phone', data);
        this.setProperty('ic', data);
        this.setProperty('nameDelivery', data);
        this.setProperty('streetDelivery', data);
        this.setProperty('cityDelivery', data);
        this.setProperty('zipDelivery', data);
    }
}
exports.Client = Client;
