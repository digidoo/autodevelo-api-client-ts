import { Entity } from "./Entity";
import { DayOpeningHours } from "./DayOpeningHours";
export declare class RentOpeningHoursWeek extends Entity {
    monday: DayOpeningHours;
    tuesday: DayOpeningHours;
    wednesday: DayOpeningHours;
    thursday: DayOpeningHours;
    friday: DayOpeningHours;
    saturday: DayOpeningHours;
    sunday: DayOpeningHours;
    constructor(data: object);
}
