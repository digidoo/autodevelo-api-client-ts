"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Entity_1 = require("./Entity");
class DayOpeningHours extends Entity_1.Entity {
    constructor(data) {
        super(data);
        this.setBoolProperty('closed', data);
        this.setProperty('from', data);
        this.setProperty('to', data);
    }
}
exports.DayOpeningHours = DayOpeningHours;
