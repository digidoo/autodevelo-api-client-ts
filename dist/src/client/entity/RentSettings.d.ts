import { Entity } from "./Entity";
import { RentOpeningHours } from "./RentOpeningHours";
export declare class RentSettings extends Entity {
    openingHours: RentOpeningHours;
    constructor(data: object);
}
