export declare class Entity {
    id?: number;
    constructor(data: object);
    protected setProperty(name: string, data: object, propertyName?: string): void;
    protected setBoolProperty(name: string, data: object): void;
    protected isNullOrUndefined(value: any): boolean;
}
