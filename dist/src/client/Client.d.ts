import { IConnection } from "./IConnection";
export declare class Client {
    private clientId;
    private clientSecret;
    private url;
    private conneciton;
    constructor(url: string, clientId: number, clientSecret: string);
    protected dateToString(value: Date): string;
    protected getConnection(): IConnection;
}
