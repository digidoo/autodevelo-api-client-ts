"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = require("./Client");
const entity_1 = require("./entity");
class CountryClient extends Client_1.Client {
    getAll(select) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                const query = 'query { countries { ' + select + ' } }';
                return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query })
                    .then((carData) => {
                    let countries = new Array();
                    carData['data']['countries'].map(data => {
                        countries.push(new entity_1.Country(data));
                    });
                    resolve(countries);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
}
exports.CountryClient = CountryClient;
