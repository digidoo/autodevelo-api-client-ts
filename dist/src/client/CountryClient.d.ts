import { Client } from "./Client";
import { Country } from "./entity";
export declare class CountryClient extends Client {
    getAll(select: string): Promise<Array<Country>>;
}
