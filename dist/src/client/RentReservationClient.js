"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = require("./Client");
const GraphqlQueryBuilder_1 = require("../Graphql/GraphqlQueryBuilder");
const RentReservation_1 = require("./entity/RentReservation");
class RentReservationClient extends Client_1.Client {
    readReservation(select, id) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = GraphqlQueryBuilder_1.GraphqlQueryBuilder.createGplGuery('rent.getReserveById', [select], {
                id: id
            });
            return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query }).then((data) => {
                return new RentReservation_1.RentReservation(data['data']['rent']['getReserveById']);
            });
        });
    }
    readConfirmedReservations(select, carId, from, to) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.readReservations(select, carId, from, to, true);
        });
    }
    readUnconfirmedReservations(select, carId, from, to) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.readReservations(select, carId, from, to, false);
        });
    }
    downloadTocPdf(reserveId) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = 'query { rent { getTocPdf(id: ' + reserveId + ') } }';
            return this.getConnection().authorizedDownload('/api/graphql', {
                query: query,
            });
        });
    }
    downloadReservePdf(reserveId) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = 'query { rent { getReservePdf(id: ' + reserveId + ') } }';
            return this.getConnection().authorizedDownload('/api/graphql', {
                query: query,
            });
        });
    }
    downloadInvoicePdf(reserveId, invoiceId) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = 'query { rent { getInvoicePdf(id: ' + reserveId + ', invoiceId: ' + invoiceId + ' ) } }';
            return this.getConnection().authorizedDownload('/api/graphql', {
                query: query,
            });
        });
    }
    createReservation(select, carId, services, destinationCountryId, locationRentId, locationReturnId, travelersCount, freeParking, rentFrom, rentTo, clientCountryId, clientCompany, clientName, clientSurname, clientStreet, clientNumber, clientCity, clientZip, clientEmail, clientPhone, payTypeId, payTypeDepositId, payTypeBailId, isCarDelivery, isCarTakeover, clientIc, note, clientNameDelivery, clientStreetDelivery, clientCityDelivery, clientZipDelivery) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                let args = {};
                args['rentFrom'] = this.dateToString(rentFrom);
                args['rentTo'] = this.dateToString(rentTo);
                args['idCar'] = carId;
                args['idDestination'] = destinationCountryId;
                args['idLocationRent'] = locationRentId;
                args['idLocationReturn'] = locationReturnId;
                args['freeParking'] = freeParking;
                args['travelers'] = travelersCount;
                args['idPayType'] = payTypeId;
                args['idPayTypeDeposit'] = payTypeDepositId;
                args['idPayTypeBail'] = payTypeBailId;
                args['isCarDelivery'] = isCarDelivery;
                args['isCarTakeover'] = isCarTakeover;
                args['note'] = note;
                args['services'] = services;
                args['client'] = {};
                args['client']['idCountry'] = clientCountryId;
                args['client']['company'] = clientCompany;
                args['client']['name'] = clientName;
                args['client']['surname'] = clientSurname;
                args['client']['street'] = clientStreet;
                args['client']['number'] = clientNumber;
                args['client']['city'] = clientCity;
                args['client']['zip'] = clientZip;
                args['client']['email'] = clientEmail;
                args['client']['phone'] = clientPhone;
                args['client']['ic'] = clientIc;
                args['client']['nameDelivery'] = clientNameDelivery;
                args['client']['streetDelivery'] = clientStreetDelivery;
                args['client']['cityDelivery'] = clientCityDelivery;
                args['client']['zipDelivery'] = clientZipDelivery;
                let query = 'mutation($input: ReservationInput!) { ' +
                    'rentMutation {' +
                    'createReservation(input: $input) { ' +
                    select +
                    ' } ' +
                    ' } ' +
                    '}';
                return this.getConnection().sendAuthorizationRequest('/api/graphql', {
                    query: query,
                    variables: JSON.stringify({ input: args })
                })
                    .then((data) => {
                    resolve(new RentReservation_1.RentReservation(data['data']['rentMutation']['createReservation']));
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
    readReservations(select, carId, from, to, confirmed) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                let args = {
                    confirmed: confirmed,
                    carId: carId,
                };
                if (from !== undefined) {
                    args['from'] = this.dateToString(from);
                }
                if (to !== undefined) {
                    args['to'] = this.dateToString(to);
                }
                const query = GraphqlQueryBuilder_1.GraphqlQueryBuilder.createGplGuery('rent.readReservationsByCar', select, args);
                console.log(query);
                return this.getConnection().sendAuthorizationRequest('/api/graphql', { query: query })
                    .then((data) => {
                    const cars = this.createRentCarsFromData(data);
                    resolve(cars);
                }).catch(err => {
                    reject(err);
                });
            });
        });
    }
    createRentCarsFromData(data) {
        let reservations = new Array();
        data['data']['rent']['readReservationsByCar'].map((value) => {
            let reservation = new RentReservation_1.RentReservation(value);
            reservations.push(reservation);
        });
        return reservations;
    }
}
exports.RentReservationClient = RentReservationClient;
