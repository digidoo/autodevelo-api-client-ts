import { Client } from "./Client";
import { Car } from "./entity/Car";
export declare class CarClient extends Client {
    getCarById(select: Array<string>, id: number): Promise<Car>;
}
