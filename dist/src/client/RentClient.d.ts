import { Client } from "./Client";
import { Car } from "./entity/Car";
import { CarList } from "./entity/CarList";
import { RentCarType } from "./entity/RentCarType";
import { CarLocation } from "./entity";
import { RentSettings } from "./entity/RentSettings";
export declare class RentClient extends Client {
    readRentSettings(select: string): Promise<RentSettings>;
    readAvailableCarLocations(select: string): Promise<Array<CarLocation>>;
    readAvailableRentCarTypes(select: string): Promise<Array<RentCarType>>;
    readAvailableRentCarEquipments(select: string): Promise<any[]>;
    getCarById(select: Array<string>, id: number, from: Date, to: Date): Promise<Car>;
    readAvailableRentCar(select: Array<string>, from?: Date, to?: Date, limit?: number, offset?: number, seats?: number, types?: Array<string>, equipmentIds?: Array<number>, locationId?: number): Promise<CarList>;
    readUnavailableRentCar(select: Array<string>, from?: Date, to?: Date, limit?: number, offset?: number, seats?: number, types?: Array<string>, equipmentIds?: Array<number>, locationId?: number): Promise<CarList>;
    protected readRentCar(queryName: string, select: Array<string>, from?: Date, to?: Date, limit?: number, offset?: number, seats?: number, types?: Array<string>, equipmentIds?: Array<number>, locationId?: number): Promise<{}>;
}
