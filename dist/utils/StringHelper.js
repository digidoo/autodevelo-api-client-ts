"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class StringHelper {
    static isANumber(str) {
        return !/\D/.test(str);
    }
}
exports.StringHelper = StringHelper;
